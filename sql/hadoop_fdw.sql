/*-------------------------------------------------------------------------
 *
 *                foreign-data wrapper  Hive 
 *
 * Copyright (c) 2012-2013, BigSQL Development Group
 *
 * IDENTIFICATION
 *                hadoop_fdw/=sql/hadoop_fdw.sql
 *
 *-------------------------------------------------------------------------
 */

CREATE FUNCTION hadoop_fdw_handler()
RETURNS fdw_handler
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

CREATE FUNCTION hadoop_fdw_validator(text[], oid)
RETURNS void
AS 'MODULE_PATHNAME'
LANGUAGE C STRICT;

CREATE FUNCTION hadoop_fdw_create_table(text, text, text)
RETURNS bool
AS 'MODULE_PATHNAME','hadoop_fdw_create_table'
LANGUAGE C STRICT;

CREATE FUNCTION hadoop_fdw_create_table(text, text)
RETURNS bool
AS 'MODULE_PATHNAME','hadoop_fdw_create_table'
LANGUAGE C STRICT;

CREATE FUNCTION hadoop_fdw_create_view(text, text, text, text)
RETURNS bool
AS 'MODULE_PATHNAME','hadoop_fdw_create_view'
LANGUAGE C STRICT;

CREATE FUNCTION hadoop_fdw_create_view(text, text, text)
RETURNS bool
AS 'MODULE_PATHNAME','hadoop_fdw_create_view'
LANGUAGE C STRICT;

CREATE FOREIGN DATA WRAPPER hadoop_fdw
  HANDLER hadoop_fdw_handler
  VALIDATOR hadoop_fdw_validator;
