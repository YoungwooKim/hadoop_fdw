#=============================================
# Hadoop Foreign Data Wrapper for PostgreSQL
#=============================================


Instruction for Building from Source
-------------------------------

1.) Download and build THRIFT Components

    Hadoop FDW requires thrift v0.9.1 (http://thrift.apache.org/)

    Make sure you have reviewed and run the THRIFT pre-requisites to compile
    http://thrift.apache.org/docs/install/

    Unzip the downloaded thrift tarball in your home directory and set THRIFT_HOME environment variable

       $ cd $HOME
       $ tar -xvf thrift-0.9.1.tar.gz
       $ export THRIFT_HOME=$HOME/thrift-0.9.1
       $ cd $THRIFT_HOME


    Set compile flag needed for building thrift and for fb303
       $ export CXXFLAGS="-fPIC"


    Configure thrift for compiling on your platform
       $ ./configure --prefix=$THRIFT_HOME --without-csharp --without-java --without-erlang --without-python --without-perl --without-php --without-ruby --without-haskell --without-go


    build THRIFT 
       $ make
       $ make install

    build FB303
       $ cd contrib/fb303
       $ ./bootstrap.sh
       $ export CPPFLAGS="-I$THRIFT_HOME/include -fPIC"
       $ ./configure --prefix=$THRIFT_HOME --with-thriftpath=$THRIFT_HOME
       $ make
       $ sudo make install


2.) Download and build PostgreSQL 9.3 from source at (http://www.postgresql.org/ftp/source/)
    Unzip the downloaded postgresql tarball in your home directory

       $ cd $HOME
       $ tar -xvf postgresql-9.3rc1.tar.gz
       $ cd ~/postgresql-9.3rc1
       $ ./configure --prefix=$PWD --enable-depend --enable-cassert --enable-debug
       $ make
       $ make install

    Put the bin directory of this postgresql build at the front of your path and test if
    correct

       $ export PATH=$PWD/bin:$PATH
       $ psql --version
            psql (PostgreSQL) 9.3rc1



3.) Get the latest version of HadoopFDW source into a directory named hadoop_fdw under the
    contrib directory of postgresql.   You'll need the THRIFT_HOME, CPP_FLAGS and CXXFLAGS
    environment variables set as per the previous steps.   Note that you'll need your own
    "userid" for pulling from bitbucket.org.

       $ cd contrib
       $ git clone https://userid@bitbucket.org/openscg/hadoop_fdw.git

       $ make clean-hive-client
       $ make hive_client

       $ make clean-hbase-client
       $ make hbase_client

       $ make
       $ make install


4.) Get a copy of Hadoop, Hive & HBase from http://www.bigsql.org

       $ tar -xvf bigsql-9.3rc1-42-linux64.tar.bz2
       $ cd bigsql-9.3rc1-42
       $ ./bigsql start

     You can now create the "example.customer_history" table in Hive

       $ cd examples/test
       $ ./testAll.sh


5.) You are now ready to use this PostgreSQL Foreign Data Wrapper to connect to Hadoop.
    First, initialize and start postgresql on port 5433 so it won't conflict with BigSQL's 
    metastore PG instance that runs on port 5432

      $ cd ../../bin
      $ ./initdb -D ../data
      $ ./pg_ctl start -D ../data -o "-p 5433" -l logfile

    Next run psql & setup the FDW

      $ ./psql -U user -p 5433 postgres

        postgres=# CREATE EXTENSION hadoop_fdw;

        postgres=# 
          CREATE SERVER hadoop_server FOREIGN DATA WRAPPER hadoop_fdw 
            OPTIONS (address '127.0.0.1', port '10000');

        postgres=# CREATE USER MAPPING FOR PUBLIC SERVER hadoop_server;
    
    Then create & query the foreign table

        postgres=# 
          CREATE FOREIGN TABLE customer_history (
            hist_id  INT,  
            h_c_id   INT,
            h_c_d_id INT,
            h_c_w_id INT,
            h_d_id   INT,
            h_w_id   INT,
            h_date   TIMESTAMP,
            h_amount DECIMAL,
            h_data   TEXT )
          SERVER hadoop_server
          OPTIONS (table 'example.customer_history');

        postgres=# SELECT hist_id, h_date, h_amount FROM customer_history LIMIT 5;

           hist_id |         h_date          | h_amount 
          ---------+-------------------------+----------
                 1 | 2013-08-13 03:56:56.522 |       10
                 2 | 2013-08-13 03:56:56.534 |       10
                 3 | 2013-08-13 03:56:56.534 |       10
                 4 | 2013-08-13 03:56:56.534 |       10
                 5 | 2013-08-13 03:56:56.535 |       10
          (5 rows)
