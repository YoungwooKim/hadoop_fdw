/*-------------------------------------------------------------------------
 *
 * sample.cpp
 * 
 * Copyright (c) 2012-2013, BigSQL Development Group
 *
 * IDENTIFICATION
 *                hadoop_fdw/sample/sample.cpp
 *
 *
 * @file sample.cpp
 * @brief Contains a sample program for test purposes. It connects with
 *        HiveServer2 and fetches list of functions and prints it to console
 *
-------------------------------------------------------------------------*/

#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/algorithm/string.hpp>
#include <protocol/TBinaryProtocol.h>
#include <transport/TTransportUtils.h>
#include <transport/TTransport.h>
#include <transport/TSocket.h>

#include "TCLIService.h"
#include "ThriftHive.h"

using namespace std;
using namespace boost;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;
using namespace apache::hive::service::cli::thrift;

int main(int argc, char **argv) {

        const int MAX_HIVE_ERR_MSG_LEN = 128;

	bool			verbose = false; 		// Set it to true to view some extra output
	char                    *svr_address = "localhost";
	int                     svr_port = 10000;
	char                    *svr_database = NULL;
	char                    *svr_table = NULL;
	char                    *query = NULL;
	char                    err_buf[MAX_HIVE_ERR_MSG_LEN];
	
	string query_str = "SHOW FUNCTIONS";

  	shared_ptr<TSocket> socket(new TSocket(svr_address, svr_port));
  	shared_ptr<TTransport> transport;

    	shared_ptr<TBufferedTransport> bufferedSocket(new TBufferedTransport(socket));
    	transport = bufferedSocket;

  	shared_ptr<TBinaryProtocol> protocol(new TBinaryProtocol(transport));
  	shared_ptr<TCLIServiceClient> client(new TCLIServiceClient(protocol));


  	try {

    		transport->open();

		cout << "\nQuery: " << query_str << endl;
		
		TOpenSessionReq *sessionReq = new TOpenSessionReq();
        	sessionReq->__set_client_protocol((TProtocolVersion::type)0);
        	TOpenSessionResp *sessionRes = new TOpenSessionResp();
        	TSessionHandle *sessionHandle = new TSessionHandle();
        	client->OpenSession(*sessionRes, *sessionReq);
        	TExecuteStatementReq *execStmtReq = new TExecuteStatementReq();
        	execStmtReq->__set_sessionHandle(sessionRes->sessionHandle);
        	execStmtReq->__set_statement(query_str);
        	TExecuteStatementResp *execStmtRes = new TExecuteStatementResp();

        	client->ExecuteStatement(*execStmtRes, *execStmtReq);
		if (verbose)
		{	
			cout << "\nExecution Debug Info:" << endl;	
        		cout<< "\tExecute Error Message:" << execStmtRes->status.errorMessage <<endl;
        		cout<< "\tExecute Error Code:" << execStmtRes->status.errorCode<<endl;
        		cout<< "\tExecute SQLState:" << execStmtRes->status.sqlState<<endl;
        		cout<< "\tExecute Operation handle has results: " << (execStmtRes->operationHandle.hasResultSet ? "TRUE" : "FALSE") << endl;
		}
        	TFetchResultsReq *fetchReq = new TFetchResultsReq();
       	 	fetchReq->__set_operationHandle(execStmtRes->operationHandle);
        	fetchReq->__set_orientation((TFetchOrientation::type)0);
        	fetchReq->maxRows = 10000;


        	TFetchResultsResp *fetchRes = new TFetchResultsResp();
        	client->FetchResults(*fetchRes, *fetchReq);
		
		if(verbose)
		{
        		cout<< "\tFetch Response Error Message: " << fetchRes->status.errorMessage << endl;
        		cout<< "\tFetch Response Error Code: " << fetchRes->status.errorCode << endl;
        		cout<< "\tFetch Response SQLState: " << fetchRes->status.sqlState << endl;
		}

        	std::vector<TRow> vRow = fetchRes->results.rows;

        	if (verbose)
		{
			cout<<"Total Rows Received: "<< vRow.size() <<endl;
		}

		cout<<"\nResults:" << endl; 

        	for (std::vector<TRow>::iterator rIt = vRow.begin(); rIt != vRow.end(); rIt++)
        	{
                	std::vector<TColumnValue> vCol = rIt->colVals;
                	for (std::vector<TColumnValue>::iterator cIt = vCol.begin(); cIt != vCol.end(); cIt++)
                	{
                        	cout << "\t" << cIt->stringVal.value;
                	}
                	cout << endl;
        	}

	       	TCloseOperationReq *clreq = new TCloseOperationReq();
		clreq->__set_operationHandle(execStmtRes->operationHandle);
		TCloseOperationResp *clres = new TCloseOperationResp();
		client->CloseOperation(*clres, *clreq); 
		TCloseSessionReq *clsreq = new TCloseSessionReq();
		clsreq->__set_sessionHandle(sessionRes->sessionHandle);
		TCloseSessionResp *clsres = new TCloseSessionResp();
		client->CloseSession(*clsres, *clsreq);
  	} catch (Apache::Hadoop::Hive::HiveServerException& ex) {
    		cerr << "Exception:" << ex.what();
		transport->close();
		return 1;
  	} catch (TTransportException& ttx) {
    		cerr << "Exception:" << ttx.what() << endl;
		return 1; 
	 } catch (...) {
      		cerr<< "Some unknown exception occurred!!" << endl;
		transport->close();
		return 1;
  	}

		transport->close();
	cout << "\nExecution completed successfully!!" << endl;
 	return 0;
}


