/*-------------------------------------------------------------------------
 *
 * hadoop_fdw.c
 *                Foreign-data wrapper for Hadoop
 *
 * Copyright (c) 2012-2013, BigSQL Development Group
 * Portions Copyright (c) 2012, PostgreSQL Global Development Group
 *
 * IDENTIFICATION
 *                hadoop_fdw/src/hadoop_fdw.c
 *
 *-------------------------------------------------------------------------
 */

#include "postgres.h"

#include "miscadmin.h"

#include <limits.h>

#include "hadoop_fdw.h"
#include "flume_client.h"

#include "funcapi.h"
#include "access/reloptions.h"
#include "access/sysattr.h"
#include "executor/spi.h"
#include "foreign/fdwapi.h"
#include "optimizer/restrictinfo.h"
#include "catalog/pg_foreign_server.h"
#include "catalog/pg_foreign_table.h"
#include "commands/explain.h"
#include "commands/vacuum.h"
#include "nodes/makefuncs.h"
#include "nodes/nodeFuncs.h"
#include "optimizer/var.h"
#include "parser/parsetree.h"
#include "utils/acl.h"
#include "utils/builtins.h"
#include "utils/guc.h"
#include "utils/lsyscache.h"
#include "utils/memutils.h"

#include "hive/hiveclient.h"
#include "hbase/HbaseClient.h"

PG_MODULE_MAGIC;

/* Default cost to start up a hadoop foreign query. */
#define DEFAULT_FDW_STARTUP_COST        10000.0

/* structures used by the FDW */

/*
 * Describes the valid options for objects that use this wrapper.
 */
struct hadoopFdwOption
{
	const char *optname;
	Oid	   optcontext;		/* Oid of catalog in which option may appear */
};

static struct hadoopFdwOption valid_options[] =
{

	/* Connection options */
	{ "address",    ForeignServerRelationId },
	{ "port",       ForeignServerRelationId },
	{ "rowscanthreshold",	ForeignServerRelationId },
	{ "database",   ForeignTableRelationId },
	{ "table",      ForeignTableRelationId },
	{ "rowscanthreshold",      ForeignTableRelationId },
	{ "flume_address",      ForeignTableRelationId },
	{ "flume_port",         ForeignTableRelationId },
	{ "hbase_address",      ForeignTableRelationId },
	{ "hbase_port",         ForeignTableRelationId },
	{ "hbase_mapping",         ForeignTableRelationId },

	/* Sentinel */
	{ NULL,			InvalidOid }
};

typedef struct hadoopFdwRelationInfo
{
	/* baserestrictinfo clauses, broken down into safe and unsafe subsets. */
	List       *remote_conds;
	List       *local_conds;

	/* Bitmap of attr numbers we need to fetch from the remote server. */
	Bitmapset  *attrs_used;

	/* Options extracted from catalogs. */
	double		rowscanthreshold;

	/* Cached catalog information. */
	ForeignTable *table;
	ForeignServer *server;
} hadoopFdwRelationInfo;

typedef struct hadoopFdwExecutionState
{
	HiveConnection	*conn;
	HiveResultSet	*result;
	char		*query;
	List	    *retrieved_attrs; /* list of retrieved attribute numbers */
} hadoopFdwExecutionState;

typedef struct hadoopFdwModifyState
{
	Relation			rel;
	FlumeConnection	   *fl_conn;
	HbaseConnection	   *hb_conn;
	List			   *target_attrs;
	int					p_nums;
	FmgrInfo		   *p_flinfo;
	char			   *data;
	bool				use_flume;
	bool				use_hbase;
	char			   *hbase_mapping;
	char			   *hbase_table;
} hadoopFdwModifyState;

/*
 * Custom gucs..
 */
static int	hadoop_rowscanthreshold;		/* row scan threshold for WHERE clauses */
static int	hive_version;					/* specifies the HiveServer version to expect */
int version_idx;

void _PG_init(void);
/*
 * SQL functions
 */
extern Datum hadoop_fdw_handler(PG_FUNCTION_ARGS);
extern Datum hadoop_fdw_validator(PG_FUNCTION_ARGS);
extern Datum hadoop_fdw_create_table(PG_FUNCTION_ARGS);
extern Datum hadoop_fdw_create_view(PG_FUNCTION_ARGS);

PG_FUNCTION_INFO_V1(hadoop_fdw_handler);
PG_FUNCTION_INFO_V1(hadoop_fdw_validator);
PG_FUNCTION_INFO_V1(hadoop_fdw_create_table);
PG_FUNCTION_INFO_V1(hadoop_fdw_create_view);


static void hadoopGetForeignRelSize(PlannerInfo *root,
						   RelOptInfo *baserel,
						   Oid foreigntableid);

static void hadoopGetForeignPaths(PlannerInfo *root,
						 RelOptInfo *baserel,
						 Oid foreigntableid);

static ForeignScan *hadoopGetForeignPlan(PlannerInfo *root,
						RelOptInfo *baserel,
						Oid foreigntableid,
						ForeignPath *best_path,
						List *tlist,
						List *scan_clauses);

static void hadoopBeginForeignScan(ForeignScanState *node,
						  int eflags);

static TupleTableSlot *hadoopIterateForeignScan(ForeignScanState *node);

static void hadoopReScanForeignScan(ForeignScanState *node);

static void hadoopEndForeignScan(ForeignScanState *node);

static void hadoopExplainForeignScan(ForeignScanState *node,
							struct ExplainState *es);
static bool hadoopAnalyzeForeignTable(Relation relation,
							 AcquireSampleRowsFunc *func,
							 BlockNumber *totalpages);
static List *hadoopPlanForeignModify(PlannerInfo *root,
						  ModifyTable *plan,
						  Index resultRelation,
						  int subplan_index);
static void hadoopBeginForeignModify(ModifyTableState *mtstate,
						   ResultRelInfo *resultRelInfo,
						   List *fdw_private,
						   int subplan_index,
						   int eflags);
static TupleTableSlot *hadoopExecForeignInsert(EState *estate,
						  ResultRelInfo *resultRelInfo,
						  TupleTableSlot *slot,
						  TupleTableSlot *planSlot);
static void hadoopAddForeignUpdateTargets(Query *parsetree,
								RangeTblEntry *target_rte,
								Relation target_relation);
static TupleTableSlot *hadoopExecForeignUpdate(EState *estate,
						  ResultRelInfo *resultRelInfo,
						  TupleTableSlot *slot,
						  TupleTableSlot *planSlot);
static TupleTableSlot *hadoopExecForeignDelete(EState *estate,
						  ResultRelInfo *resultRelInfo,
						  TupleTableSlot *slot,
						  TupleTableSlot *planSlot);
static void hadoopEndForeignModify(EState *estate,
						 ResultRelInfo *resultRelInfo);
static void hadoopExplainForeignModify(ModifyTableState *mtstate,
							 ResultRelInfo *rinfo,
							 List *fdw_private,
							 int subplan_index,
							 ExplainState *es);
static bool hadoopIsDMLCapable(Oid foreigntableid);

/*
 * Helper functions
 */
static bool hadoopIsValidOption(const char *option, Oid context);

static void hadoopGetOptions(Oid foreigntableid, char **address, 
                           int *port, char **database, char **table);
static void hadoopGetServerOptions(Oid serverid, char **address,
								 int *port, char **database);

static int hadoopAcquireSampleRowsFunc(Relation relation, int elevel,
							  HeapTuple *rows, int targrows,
							  double *totalrows,
							  double *totaldeadrows);
static long hadoopGetInfoDescribe(HiveConnection *conn, char *describe_query,
					   char *key, bool *found);
static void hadoopExecuteSPI(char *query);
static void hadoopGetFlumeOptions(Oid foreigntableid, char **flume_address,
                                char **flume_port, char **table);
static void hadoopGetDataValues(StringInfo buf, hadoopFdwModifyState *fmstate,
                              TupleTableSlot *slot);
static void hadoopGetHbaseOptions(Oid foreigntableid, char **hbase_address,
					char **hbase_port, char **hbase_mapping, char **table);
static void hadoopGetHbaseValues(hadoopFdwModifyState *fmstate, TupleTableSlot *slot,
				  char **rowid, char ***hbase_cols, char ***hbase_vals, int *nvals);

/*
 * Arrays of function pointers where functions are called
 * based on HiveServer version
 */

HiveConnection* (*DBOpenConnection_table[])() = {DBOpenConnection, DBOpenConnection2};
HiveReturn (*DBCloseConnection_table[])() = {DBCloseConnection, DBCloseConnection2};
HiveReturn (*DBExecute_table[])() = {DBExecute, DBExecute2};
HiveReturn (*DBCloseResultSet_table[])() = {DBCloseResultSet, DBCloseResultSet2};
HiveReturn (*DBFetch_table[])() = {DBFetch, DBFetch2};
HiveReturn (*DBGetColumnCount_table[])() = {DBGetColumnCount, DBGetColumnCount2};
HiveReturn (*DBGetFieldDataLen_table[])() = {DBGetFieldDataLen, DBGetFieldDataLen2};
HiveReturn (*DBGetFieldAsCString_table[])() = {DBGetFieldAsCString, DBGetFieldAsCString2};


/*
 * _PG_init()	- library load-time initialization
 */
void
_PG_init(void)
{
	/* Be sure we do initialization only once (should be redundant now) */
	static bool inited = false;

	if (inited)
		return;

	/*
	 * Define (or redefine) custom GUC variables.
	 */
	DefineCustomIntVariable("hadoop_fdw.hadoop_rowscanthreshold",
	  "The threshold limit to decide if WHERE clauses should be evaluated locally",
	  "If the Hive table has less than or equal to these number of rows, then the WHERE"
	  " clauses are evaluated locally and not sent to Hive",
							&hadoop_rowscanthreshold,
							10000,
							0,
							INT_MAX,
							PGC_USERSET,
							0,
							NULL,
							NULL,
							NULL);

	DefineCustomIntVariable("hadoop_fdw.hive_version",
	  "It specifies what version of HiveServer to expect",
	  "Supported versions include HiveServer1 and HiveServer2. Default is 2"
	  ". Valid values are 1 and 2",
							&hive_version,
							2,
							0,
							INT_MAX,
							PGC_USERSET,
							0,
							NULL,
							NULL,
							NULL);
	EmitWarningsOnPlaceholders("hadoop_fdw");

	inited = true;
}

Datum
hadoop_fdw_handler(PG_FUNCTION_ARGS)
{
	FdwRoutine *fdwroutine = makeNode(FdwRoutine);

	elog(DEBUG2,"Start: hadoop_fdw_handler");

	/* assign the handlers for the FDW */

	/* these are required */
	fdwroutine->GetForeignRelSize = hadoopGetForeignRelSize;
	fdwroutine->GetForeignPaths = hadoopGetForeignPaths;
	fdwroutine->GetForeignPlan = hadoopGetForeignPlan;
	fdwroutine->BeginForeignScan = hadoopBeginForeignScan;
	fdwroutine->IterateForeignScan = hadoopIterateForeignScan;
	fdwroutine->ReScanForeignScan = hadoopReScanForeignScan;
	fdwroutine->EndForeignScan = hadoopEndForeignScan;

	/* remainder are optional - use NULL if not required */
	/* support for insert / update / delete */
	fdwroutine->AddForeignUpdateTargets = hadoopAddForeignUpdateTargets;
	fdwroutine->PlanForeignModify = hadoopPlanForeignModify;
	fdwroutine->BeginForeignModify = hadoopBeginForeignModify;
	fdwroutine->ExecForeignInsert = hadoopExecForeignInsert;
	fdwroutine->ExecForeignUpdate = hadoopExecForeignUpdate;
	fdwroutine->ExecForeignDelete = hadoopExecForeignDelete;
	fdwroutine->EndForeignModify = hadoopEndForeignModify;

	/* support for EXPLAIN */
	fdwroutine->ExplainForeignScan = hadoopExplainForeignScan;
	fdwroutine->ExplainForeignModify = hadoopExplainForeignModify;

	/* support for ANALYZE */
	fdwroutine->AnalyzeForeignTable = hadoopAnalyzeForeignTable;

	elog(DEBUG2,"End: hadoop_fdw_handler");

	PG_RETURN_POINTER(fdwroutine);
}

Datum
hadoop_fdw_validator(PG_FUNCTION_ARGS)
{
	List		*options_list = untransformRelOptions(PG_GETARG_DATUM(0));
	Oid			catalog = PG_GETARG_OID(1);
	char		*svr_address = NULL;
	int			svr_port = 0;
	int			flm_port = 0;
	char		*svr_database = NULL;
	char		*svr_table = NULL;
	char		*flm_address = NULL;
	ListCell	*cell;

	elog(DEBUG2,"Start: hadoop_fdw_validator");

	/*
	 * Check that only options supported by hadoop_fdw,
	 * and allowed for the current object type, are given.
	 */
	foreach(cell, options_list)
	{
		DefElem	   *def = (DefElem *) lfirst(cell);

		if (!hadoopIsValidOption(def->defname, catalog))
		{
			struct hadoopFdwOption *opt;
			StringInfoData buf;

			/*
			 * Unknown option specified, complain about it. Provide a hint
			 * with list of valid options for the object.
			 */
			initStringInfo(&buf);
			for (opt = valid_options; opt->optname; opt++)
			{
				if (catalog == opt->optcontext)
					appendStringInfo(&buf, "%s%s", (buf.len > 0) ? ", " : "",
									 opt->optname);
			}

			ereport(ERROR, 
					(errcode(ERRCODE_FDW_INVALID_OPTION_NAME),
					 errmsg("invalid option \"%s\"", def->defname),
					 errhint("Valid options in this context are: %s",
							 buf.len ? buf.data : "<none>")
					));
		}

		if (strcmp(def->defname, "address") == 0)
		{
			if (svr_address)
				ereport(ERROR,
					(errcode(ERRCODE_SYNTAX_ERROR),
					 errmsg("conflicting or redundant options: address (%s)",
						defGetString(def))));

			svr_address = defGetString(def);
		}
		else if (strcmp(def->defname, "port") == 0)
		{
			if (svr_port)
				ereport(ERROR, 
					(errcode(ERRCODE_SYNTAX_ERROR),
					 errmsg("conflicting or redundant options: port (%s)",
						defGetString(def))));

			svr_port = atoi(defGetString(def));
		}
		else if (strcmp(def->defname, "flume_address") == 0)
		{
			if (flm_address)
				ereport(ERROR,
					(errcode(ERRCODE_SYNTAX_ERROR),
					 errmsg("conflicting or redundant options: "
							"flume_address (%s)",
						defGetString(def))));

			flm_address = defGetString(def);
		}
		else if (strcmp(def->defname, "database") == 0)
		{
			if (svr_database)
				ereport(ERROR,
					(errcode(ERRCODE_SYNTAX_ERROR),
					 errmsg("conflicting or redundant options: database (%s)",
						defGetString(def))));

			svr_database = defGetString(def);
		}
		else if (strcmp(def->defname, "table") == 0)
		{
			if (svr_table)
				ereport(ERROR,
					(errcode(ERRCODE_SYNTAX_ERROR),
					 errmsg("conflicting or redundant options: table (%s)",
						defGetString(def))));

			svr_table = defGetString(def);
		}
		else if (strcmp(def->defname, "flume_port") == 0)
		{
			if (flm_port)
				ereport(ERROR,
					(errcode(ERRCODE_SYNTAX_ERROR),
					 errmsg("conflicting or redundant options: flume_port (%s)",
						defGetString(def))));

			flm_port = atoi(defGetString(def));
		}
	}

	elog(DEBUG2,"End: hadoop_fdw_validator");

	PG_RETURN_VOID();
}

/*
 * Check if the provided option is a valid one.
 */
static bool
hadoopIsValidOption(const char *option, Oid context)
{
	struct hadoopFdwOption *opt;

	elog(DEBUG2,"Start: hadoopIsValidOption");

	for (opt = valid_options; opt->optname; opt++)
	{
		if (context == opt->optcontext && strcmp(opt->optname, option) == 0)
			return true;
	}

	elog(DEBUG2,"End: hadoopIsValidOption");

	return false;
}

static void
hadoopGetForeignRelSize(PlannerInfo *root,
						   RelOptInfo *baserel,
						   Oid foreigntableid)
{
	/*
	 * Obtain relation size estimates for a foreign table. This is called at
	 * the beginning of planning for a query that scans a foreign table. root
	 * is the planner's global information about the query; baserel is the
	 * planner's information about this table; and foreigntableid is the
	 * pg_class OID of the foreign table. (foreigntableid could be obtained
	 * from the planner data structures, but it's passed explicitly to save
	 * effort.)
	 *
	 * This function should update baserel->rows to be the expected number of
	 * rows returned by the table scan, after accounting for the filtering
	 * done by the restriction quals. The initial value of baserel->rows is
	 * just a constant default estimate, which should be replaced if at all
	 * possible. The function may also choose to update baserel->width if it
	 * can compute a better estimate of the average result row width.
	 */

	hadoopFdwRelationInfo	*fpinfo;
	ListCell		*lc;
	bool			 exec_remote = false;

	elog(DEBUG2,"Start: hadoopGetForeignRelSize");

	baserel->rows = 1000;

	fpinfo = (hadoopFdwRelationInfo *) palloc0(sizeof(hadoopFdwRelationInfo));
	baserel->fdw_private = (void *) fpinfo;

	/* Look up foreign-table catalog info. */
	fpinfo->table = GetForeignTable(foreigntableid);
	fpinfo->server = GetForeignServer(fpinfo->table->serverid);

	foreach(lc, fpinfo->server->options)
	{
		DefElem    *def = (DefElem *) lfirst(lc);

		if (strcmp(def->defname, "rowscanthreshold") == 0)
			fpinfo->rowscanthreshold = strtod(defGetString(def), NULL);
	}

	foreach(lc, fpinfo->table->options)
	{
		DefElem    *def = (DefElem *) lfirst(lc);

		/* The table level value over rides server level value */
		if (strcmp(def->defname, "rowscanthreshold") == 0)
			fpinfo->rowscanthreshold = strtod(defGetString(def), NULL);
	}

	/* if rowscanthreshold is still 0, use the guc value */
	if (fpinfo->rowscanthreshold == 0)
		fpinfo->rowscanthreshold = hadoop_rowscanthreshold;
        
        elog(DEBUG1,"Using rowscanthreshold: %f", fpinfo->rowscanthreshold);
	
    /*
	 * Execute clause remotely if the number of rows in the
	 * relation are more than rowscanthreshold. Obviously the
	 * clause should be remote shippable as well
	 */
	exec_remote = (baserel->tuples >= fpinfo->rowscanthreshold);

	foreach(lc, baserel->baserestrictinfo)
	{
		RestrictInfo *ri = (RestrictInfo *) lfirst(lc);

		if (exec_remote &&
					is_foreign_expr(root, baserel, ri->clause))
			fpinfo->remote_conds = lappend(fpinfo->remote_conds, ri);
		else
			fpinfo->local_conds = lappend(fpinfo->local_conds, ri);
	}

	/*
	 * Identify which attributes will need to be retrieved from the remote
	 * server.  These include all attrs needed for joins or final output, plus
	 * all attrs used in the local_conds.
	 *
	 * In cases where there is a small amount of data, and we don't send
	 * hadoop a WHERE clause, we should also do a SELECT *. And that happens
	 * when attrs_used remains NULL for the local execution case below
	 */
	fpinfo->attrs_used = NULL;
	if (exec_remote)
	{
		pull_varattnos((Node *) baserel->reltargetlist, baserel->relid,
					   &fpinfo->attrs_used);
		foreach(lc, fpinfo->local_conds)
		{
			RestrictInfo *rinfo = (RestrictInfo *) lfirst(lc);

			pull_varattnos((Node *) rinfo->clause, baserel->relid,
						   &fpinfo->attrs_used);
		}
	}

	elog(DEBUG2,"End: hadoopGetForeignRelSize");

}

static void
hadoopGetForeignPaths(PlannerInfo *root,
						 RelOptInfo *baserel,
						 Oid foreigntableid)
{
	/*
	 * Create possible access paths for a scan on a foreign table. This is
	 * called during query planning. The parameters are the same as for
	 * GetForeignRelSize, which has already been called.
	 *
	 * This function must generate at least one access path (ForeignPath node)
	 * for a scan on the foreign table and must call add_path to add each such
	 * path to baserel->pathlist. It's recommended to use
	 * create_foreignscan_path to build the ForeignPath nodes. The function
	 * can generate multiple access paths, e.g., a path which has valid
	 * pathkeys to represent a pre-sorted result. Each access path must
	 * contain cost estimates, and can contain any FDW-private information
	 * that is needed to identify the specific scan method intended.
	 */

	/*
	 * hadoopFdwPlanState *fdw_private = baserel->fdw_private;
	 */

	Cost		startup_cost,
				total_cost;

	elog(DEBUG2,"Start: hadoopGetForeignPaths");

	startup_cost = DEFAULT_FDW_STARTUP_COST;
	total_cost = startup_cost + baserel->rows;

	/* Create a ForeignPath node and add it as only possible path */
	add_path(baserel, (Path *)
			 create_foreignscan_path(root, baserel,
									 baserel->rows,
									 startup_cost,
									 total_cost,
									 NIL,		/* no pathkeys */
									 NULL,		/* no outer rel either */
									 NIL));		/* no fdw_private data */
}



static ForeignScan *
hadoopGetForeignPlan(PlannerInfo *root,
						RelOptInfo *baserel,
						Oid foreigntableid,
						ForeignPath *best_path,
						List *tlist,
						List *scan_clauses)
{
	/*
	 * Create a ForeignScan plan node from the selected foreign access path.
	 * This is called at the end of query planning. The parameters are as for
	 * GetForeignRelSize, plus the selected ForeignPath (previously produced
	 * by GetForeignPaths), the target list to be emitted by the plan node,
	 * and the restriction clauses to be enforced by the plan node.
	 *
	 * This function must create and return a ForeignScan plan node; it's
	 * recommended to use make_foreignscan to build the ForeignScan node.
	 *
	 */
	List		*fdw_private;
	List		*remote_conds = NIL;
	List		*local_exprs = NIL;
	List		*params_list = NIL;
	List	    *retrieved_attrs;
	StringInfoData	sql;
	ListCell	*lc;

	ForeignTable *table;
	Relation	 rel;
	const char   *relname = NULL;
	Index		scan_relid = baserel->relid;
	hadoopFdwRelationInfo *fpinfo = (hadoopFdwRelationInfo *) baserel->fdw_private;

	elog(DEBUG2,"Start: hadoopGetForeignPlan");

	foreach(lc, scan_clauses)
	{
		RestrictInfo *rinfo = (RestrictInfo *) lfirst(lc);

		Assert(IsA(rinfo, RestrictInfo));

		/* Ignore any pseudoconstants, they're dealt with elsewhere */
		if (rinfo->pseudoconstant)
			continue;

		if (list_member_ptr(fpinfo->remote_conds, rinfo)) {
			remote_conds = lappend(remote_conds, rinfo);
		}
		else if (list_member_ptr(fpinfo->local_conds, rinfo))
			local_exprs = lappend(local_exprs, rinfo->clause);
		else
		{
			Assert(is_foreign_expr(root, baserel, rinfo->clause));
			remote_conds = lappend(remote_conds, rinfo);
		}
	}

	/*
	 * Build the query string to be sent for execution, and identify
	 * expressions to be sent as parameters.
	 */
	initStringInfo(&sql);

	deparseSelectSql(&sql, root, baserel, fpinfo->attrs_used,
					 &retrieved_attrs);
	appendStringInfo(&sql, "%s", " FROM ");

	rel = heap_open(foreigntableid, NoLock);
	table = GetForeignTable(RelationGetRelid(rel));

	/*
	 * Use value of FDW options if any, instead of the name of object itself.
	 */
	foreach(lc, table->options)
	{
		DefElem    *def = (DefElem *) lfirst(lc);

		if (strcmp(def->defname, "table") == 0)
			relname = defGetString(def);
	}

	appendStringInfo(&sql, "%s", relname);
	if (remote_conds)
		appendWhereClause(&sql, root, baserel, remote_conds,
						  true, &params_list);

	fdw_private = list_make2(makeString(sql.data),
							 retrieved_attrs);

	heap_close(rel, NoLock);

	/* Create the ForeignScan node */
	return make_foreignscan(tlist,
							local_exprs,
							scan_relid,
							params_list,
							fdw_private);

	elog(DEBUG2,"End: hadoopGetForeignPlan");

}


static void
hadoopBeginForeignScan(ForeignScanState *node,
						  int eflags)
{
	/*
	 * Begin executing a foreign scan. This is called during executor startup.
	 * It should perform any initialization needed before the scan can start,
	 * but not start executing the actual scan (that should be done upon the
	 * first call to IterateForeignScan). The ForeignScanState node has
	 * already been created, but its fdw_state field is still NULL.
	 * Information about the table to scan is accessible through the
	 * ForeignScanState node (in particular, from the underlying ForeignScan
	 * plan node, which contains any FDW-private information provided by
	 * GetForeignPlan). eflags contains flag bits describing the executor's
	 * operating mode for this plan node.
	 *
	 * Note that when (eflags & EXEC_FLAG_EXPLAIN_ONLY) is true, this function
	 * should not perform any externally-visible actions; it should only do
	 * the minimum required to make the node state valid for
	 * ExplainForeignScan and EndForeignScan.
	 *
	 */


	char			*svr_address = NULL;
	int				svr_port = 0;
	char			*svr_database = NULL;
	char			*svr_table = NULL;
	HiveConnection	*conn;
	hadoopFdwExecutionState	*festate;
	char			*query;
	char			err_buf[MAX_HIVE_ERR_MSG_LEN];

	ForeignScan *fsplan = (ForeignScan *) node->ss.ps.plan;

	elog(DEBUG2,"Start: hadoopBeginForeignScan");

	/* Fetch options  */
	hadoopGetOptions(RelationGetRelid(node->ss.ss_currentRelation), &svr_address, &svr_port,
				   &svr_database, &svr_table);

	/* Determine target HiveServer version */
	version_idx = hive_version - 1;
	if (version_idx < 0 || version_idx > 1)
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_ESTABLISH_CONNECTION),
				 errmsg("invalid hive version specified: %s", err_buf)
				));

	/* Connect to the server */
	conn = DBOpenConnection_table[version_idx](svr_database,
							svr_address,
							svr_port,
							atoi(DEFAULT_FRAMED),
							err_buf,
							sizeof(err_buf));

	if (!conn)
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_ESTABLISH_CONNECTION),
				 errmsg("failed to connect to Hive: %s", err_buf)
				));
	query = strVal(list_nth(fsplan->fdw_private, 0));

	elog(DEBUG1,"hadoop_fdw: Starting Query: %s", query);

	festate = (hadoopFdwExecutionState *) palloc(sizeof(hadoopFdwExecutionState));
	node->fdw_state = (void *) festate;
	festate->conn = conn;
	festate->result = NULL;
	festate->query = query;
	festate->retrieved_attrs = (List *) list_nth(fsplan->fdw_private, 1);

	elog(DEBUG2,"End: hadoopBeginForeignScan");
}

static TupleTableSlot *
hadoopIterateForeignScan(ForeignScanState *node)
{
	/*
	 * Fetch one row from the foreign source, returning it in a tuple table
	 * slot (the node's ScanTupleSlot should be used for this purpose). Return
	 * NULL if no more rows are available. The tuple table slot infrastructure
	 * allows either a physical or virtual tuple to be returned; in most cases
	 * the latter choice is preferable from a performance standpoint. Note
	 * that this is called in a short-lived memory context that will be reset
	 * between invocations. Create a memory context in BeginForeignScan if you
	 * need longer-lived storage, or use the es_query_cxt of the node's
	 * EState.
	 *
	 * The rows returned must match the column signature of the foreign table
	 * being scanned. If you choose to optimize away fetching columns that are
	 * not needed, you should insert nulls in those column positions.
	 *
	 * Note that PostgreSQL's executor doesn't care whether the rows returned
	 * violate any NOT NULL constraints that were defined on the foreign table
	 * columns — but the planner does care, and may optimize queries
	 * incorrectly if NULL values are present in a column declared not to
	 * contain them. If a NULL value is encountered when the user has declared
	 * that none should be present, it may be appropriate to raise an error
	 * (just as you would need to do in the case of a data type mismatch).
	 */


	HiveResultSet*	resultset = NULL;
	HeapTuple	tuple;
	char	   **values;
	char		err_buf[MAX_HIVE_ERR_MSG_LEN];
	size_t		col_count;
	size_t		col_len;
	size_t		data_byte_size;
	char 	   *field;
	int			is_null_value;
	int			x;

	hadoopFdwExecutionState *festate = (hadoopFdwExecutionState *) node->fdw_state;
	TupleTableSlot *slot = node->ss.ss_ScanTupleSlot;
	TupleDesc	tupdesc = node->ss.ss_currentRelation->rd_att;

	elog(DEBUG2,"Start: hadoopIterateForeignScan");

	/* Execute the query, if required */
	if (!festate->result)
	{
		if (DBExecute_table[version_idx](festate->conn,
					  festate->query,
					  &resultset,
					  1000,
					  err_buf,
					  sizeof(err_buf)) == HIVE_ERROR)
		{
			char *err = pstrdup(err_buf);
			DBCloseConnection_table[version_idx](festate->conn, err_buf, sizeof(err_buf));
			festate->conn = NULL;
			ereport(ERROR,
					(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
					 errmsg("failed to execute the Hive query: %s", err)
					));
		}
		/* Guess the query succeeded then */
		festate->result = resultset;
	}
	ExecClearTuple(slot);
	/* get the next record, if any, and fill in the slot */
	if (DBFetch_table[version_idx](festate->result,
				 err_buf,
				 sizeof(err_buf)) == HIVE_SUCCESS)
	{
		ListCell   *lc = list_head(festate->retrieved_attrs);

		/* Build the tuple */
		DBGetColumnCount_table[version_idx](festate->result, &col_count, err_buf, sizeof(err_buf));

		/* We palloc total attributes size and not what col_count says */
		values = (char **) palloc0(sizeof(char *) * tupdesc->natts);

		Assert(col_count == list_length(festate->retrieved_attrs));
		for (x = 0; x < col_count; x++)
		{
			int	i = lfirst_int(lc);

			DBGetFieldDataLen_table[version_idx](festate->result, x, &col_len, err_buf, sizeof(err_buf));

			/* ordinary column */
			Assert(i <= tupdesc->natts);

			field = (char *) palloc0(col_len + 1);
			if (DBGetFieldAsCString_table[version_idx](festate->result,
									x,
									field,
									col_len + 1,
									&data_byte_size,
									&is_null_value,
									err_buf,
									sizeof(err_buf)) != HIVE_ERROR)
			{

				if (!is_null_value)
					values[i - 1] = field;
			}
			else
			{
				char *err = pstrdup(err_buf);
				DBCloseConnection_table[version_idx](festate->conn, err_buf, sizeof(err_buf));

				festate->conn = NULL;
				ereport(ERROR,
						(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
						 errmsg("failed to execute the Hive query: %s", err)
						));

			}
			/* Advance the retrieved_attrs list pointer */
			lc = lnext(lc);
		}
		tuple = BuildTupleFromCStrings(
					TupleDescGetAttInMetadata(tupdesc), values);

		ExecStoreTuple(tuple, slot, InvalidBuffer, false);
	}

	elog(DEBUG2,"End: hadoopIterateForeignScan");

	/* then return the slot */
	return slot;
}


static void
hadoopReScanForeignScan(ForeignScanState *node)
{
	elog(DEBUG2,"Start: hadoopReScanForeignScan");

	/*
	 * Restart the scan from the beginning. Note that any parameters the scan
	 * depends on may have changed value, so the new scan does not necessarily
	 * return exactly the same rows.
	 */

	elog(DEBUG2,"End: hadoopReScanForeignScan");

}


static void
hadoopEndForeignScan(ForeignScanState *node)
{
	/*
	 * End the scan and release resources. It is normally not important to
	 * release palloc'd memory, but for example open files and connections to
	 * remote servers should be cleaned up.
	 */

	char err_buf[MAX_HIVE_ERR_MSG_LEN];

	hadoopFdwExecutionState *festate = (hadoopFdwExecutionState *) node->fdw_state;

	elog(DEBUG2,"Start: hadoopEndForeignScan");

	if (festate->result)
	{
		DBCloseResultSet_table[version_idx](festate->result, err_buf, sizeof(err_buf));

		festate->result = NULL;
	}

	if (festate->conn)
	{
		DBCloseConnection_table[version_idx](festate->conn, err_buf, sizeof(err_buf));

		festate->conn = NULL;
	}

	elog(DEBUG2,"End: hadoopEndForeignScan");

}


static void
hadoopExplainForeignScan(ForeignScanState *node,
			struct ExplainState *es)
{
	/*
	 * Print additional EXPLAIN output for a foreign table scan. This function
	 * can call ExplainPropertyText and related functions to add fields to the
	 * EXPLAIN output. The flag fields in es can be used to determine what to
	 * print, and the state of the ForeignScanState node can be inspected to
	 * provide run-time statistics in the EXPLAIN ANALYZE case.
	 *
	 * If the ExplainForeignScan pointer is set to NULL, no additional
	 * information is printed during EXPLAIN.
	 */

	List       *fdw_private;
	char       *sql;

	elog(DEBUG2,"Start: hadoopExplainForeignScan");

	if (es->verbose)
	{
		fdw_private = ((ForeignScan *) node->ss.ps.plan)->fdw_private;
		sql = strVal(list_nth(fdw_private, 0));
		ExplainPropertyText("Remote SQL", sql, es);
	}

	elog(DEBUG2,"End: hadoopExplainForeignScan");

}

static bool
hadoopAnalyzeForeignTable(Relation relation,
							 AcquireSampleRowsFunc *func,
							 BlockNumber *totalpages)
{
	/* ----
	 * This function is called when ANALYZE is executed on a foreign table. If
	 * the FDW can collect statistics for this foreign table, it should return
	 * true, and provide a pointer to a function that will collect sample rows
	 * from the table in func, plus the estimated size of the table in pages
	 * in totalpages. Otherwise, return false.
	 *
	 * If the FDW does not support collecting statistics for any tables, the
	 * AnalyzeForeignTable pointer can be set to NULL.
	 *
	 * If provided, the sample collection function must have the signature:
	 *
	 *	  int
	 *	  AcquireSampleRowsFunc (Relation relation, int elevel,
	 *							 HeapTuple *rows, int targrows,
	 *							 double *totalrows,
	 *							 double *totaldeadrows);
	 *
	 * A random sample of up to targrows rows should be collected from the
	 * table and stored into the caller-provided rows array. The actual number
	 * of rows collected must be returned. In addition, store estimates of the
	 * total numbers of live and dead rows in the table into the output
	 * parameters totalrows and totaldeadrows. (Set totaldeadrows to zero if
	 * the FDW does not have any concept of dead rows.)
	 * ----
	 */
	HiveResultSet*	resultset = NULL;
	char			err_buf[MAX_HIVE_ERR_MSG_LEN];
	char		   *svr_address = NULL;
	int				svr_port = 0;
	char		   *svr_database = NULL;
	char		   *svr_table = NULL;
	StringInfoData	sql;
	HiveConnection *conn;
	long			total_size = 0;
	bool			found = false;


	/* Return the row-analysis function pointer */
	*func = hadoopAcquireSampleRowsFunc;

	elog(DEBUG2,"Start: hadoopAnalyzeForeignTable");

	/* Determine target HiveServer version */
	version_idx = hive_version - 1;
	if (version_idx < 0 || version_idx > 1)
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_ESTABLISH_CONNECTION),
				 errmsg("invalid hive version specified: %s", err_buf)
				));

	/* Fetch options  */
	hadoopGetOptions(RelationGetRelid(relation), &svr_address, &svr_port,
				   &svr_database, &svr_table);

	/* Connect to the server */
	conn = DBOpenConnection_table[version_idx](svr_database,
							svr_address,
							svr_port,
							atoi(DEFAULT_FRAMED),
							err_buf,
							sizeof(err_buf));

	if (!conn)
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_ESTABLISH_CONNECTION),
				 errmsg("failed to connect to Hive: %s", err_buf)
				));
	/*
	 * We have to get the number of pages. For this we first run the hadoop
	 * ANALYZE command on the table. Unfortunately, that does not return
	 * any output. So we need to run a subsequent "DESCRIBE EXTENDED"
	 * command and interpret the contents to get the statistics
	 */
	initStringInfo(&sql);

	appendStringInfo(&sql, "%s %s %s", "ANALYZE TABLE", svr_table,
					 "COMPUTE STATISTICS");

	elog(DEBUG1,"hadoop_fdw: Analyzing table: %s", sql.data);

	if (DBExecute_table[version_idx](conn,
				   sql.data,
				   NULL,
				   0,
				   err_buf,
				   sizeof(err_buf)) == HIVE_ERROR)
	{
		char *err = pstrdup(err_buf);
		DBCloseConnection_table[version_idx](conn, err_buf, sizeof(err_buf));

		conn = NULL;
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
				 errmsg("failed to execute the Hive query: %s", err)
				));
	}

	/*
	 * If the above COMPUTE succeeded, give out a DESCRIBE EXTENDED
	 * query to get the statistics. Unfortunately, this query returns
	 * more data than is required, but no other way then to go through
	 * it in search of what we are looking for..
	 */
	resetStringInfo(&sql);

	appendStringInfo(&sql, "%s %s", "DESCRIBE EXTENDED", svr_table);

	elog(DEBUG1,"hadoop_fdw: Describing table: %s", sql.data);

	total_size = hadoopGetInfoDescribe(conn, sql.data,
											"totalSize", &found);

	/* cleanup resultset and the connection as well */
	if (resultset)
		DBCloseResultSet_table[version_idx](resultset, err_buf, sizeof(err_buf));

	DBCloseConnection_table[version_idx](conn, err_buf, sizeof(err_buf));

	if (!found)
		ereport(WARNING,
			(errmsg("Total size information not retrieved from hadoop...")));

	elog(DEBUG2,"End: hadoopAnalyzeForeignTable");

	*totalpages = total_size/BLCKSZ;
	return true;
}


/*
 * Acquire a random sample of rows from foreign table managed by hadoop_fdw.
 *
 * Selected rows are returned in the caller-allocated array rows[],
 * which must have at least targrows entries.
 * The actual number of rows selected is returned as the function result.
 * We also count the total number of rows in the table and return it into
 * *totalrows.	Note that *totaldeadrows is always set to 0.
 */
static int
hadoopAcquireSampleRowsFunc(Relation relation, int elevel,
							  HeapTuple *rows, int targrows,
							  double *totalrows,
							  double *totaldeadrows)
{
	int			numrows = 0;
	char	  **values;
	bool		found = false;
	char	   *svr_address = NULL;
	int			svr_port = 0;
	char	   *svr_database = NULL;
	char	   *svr_table = NULL;
	char		err_buf[MAX_HIVE_ERR_MSG_LEN];
	size_t		col_count;
	size_t		col_len;
	size_t		data_byte_size;
	char	   *field;
	int			is_null_value;
	int			col_idx;
	MemoryContext	oldcontext = CurrentMemoryContext;
	MemoryContext	tupcontext;
	StringInfoData	sql;
	HiveResultSet  *resultset = NULL;
	HiveConnection *conn;
	TupleDesc		tupDesc;

	Assert(relation);
	Assert(targrows > 0);
	elog(DEBUG2,"Start: hadoopAcquireSampleRowsFunc");

	tupDesc = RelationGetDescr(relation);
	values = (char **) palloc(tupDesc->natts * sizeof(char *));

	/* Fetch options of foreign table */
	hadoopGetOptions(RelationGetRelid(relation), &svr_address, &svr_port,
				   &svr_database, &svr_table);

	/* Connect to the server */
	conn = DBOpenConnection_table[version_idx](svr_database,
							svr_address,
							svr_port,
							atoi(DEFAULT_FRAMED),
							err_buf,
							sizeof(err_buf));

	if (!conn)
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_ESTABLISH_CONNECTION),
				 errmsg("failed to connect to Hive: %s", err_buf)
				));

	/*
	 * We have to get sample number for rows from Hive. There's
	 * TABLESAMPLE available, but we use the basic 'order by rand()'
	 * query to get sample rows from the other side
	 */
	initStringInfo(&sql);

	appendStringInfo(&sql, "SELECT * FROM %s ORDER BY RAND() LIMIT %d",
					 svr_table, targrows);

	elog(DEBUG1,"hadoop_fdw: Acquiring sample rows from table: %s", sql.data);

	if (DBExecute_table[version_idx](conn,
				sql.data,
				NULL,
				0,
				err_buf,
				sizeof(err_buf)) == HIVE_ERROR)
	{
		char *err = pstrdup(err_buf);
		DBCloseConnection_table[version_idx](conn, err_buf, sizeof(err_buf));

		conn = NULL;
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
				 errmsg("failed to execute the sampling Hive query: %s", err)
				));
	}

	/*
	 * Use per-tuple memory context to prevent leak of memory used to read
	 * rows from Hive.
	 */
	tupcontext = AllocSetContextCreate(CurrentMemoryContext,
									   "hadoop_fdw temporary context",
									   ALLOCSET_DEFAULT_MINSIZE,
									   ALLOCSET_DEFAULT_INITSIZE,
									   ALLOCSET_DEFAULT_MAXSIZE);

	while (DBFetch_table[version_idx](resultset, err_buf, sizeof(err_buf)) == HIVE_SUCCESS)
	{
		/* Check for user-requested abort or sleep */
		vacuum_delay_point();

		/* Fetch next row */
		MemoryContextReset(tupcontext);
		MemoryContextSwitchTo(tupcontext);

		col_count = tupDesc->natts;
		for (col_idx = 0; col_idx < col_count; col_idx++)
		{
			DBGetFieldDataLen_table[version_idx](resultset, col_idx, &col_len, err_buf, sizeof(err_buf));

			field = (char *) palloc0(col_len + 1);
			if (DBGetFieldAsCString_table[version_idx](resultset,
									col_idx,
									field,
									col_len + 1,
									&data_byte_size,
									&is_null_value,
									err_buf,
									sizeof(err_buf)) != HIVE_ERROR)
			{

				if (is_null_value)
					values[col_idx] = NULL;
				else
					values[col_idx] = field;
			}
			else
			{
				char *err = pstrdup(err_buf);
				DBCloseConnection_table[version_idx](conn, err_buf, sizeof(err_buf));
				conn = NULL;
				ereport(ERROR,
						(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
						 errmsg("failed to fetch next sample Hive query row: %s", err)
						));

			}
		}

		MemoryContextSwitchTo(oldcontext);
		rows[numrows++] = BuildTupleFromCStrings(
					TupleDescGetAttInMetadata(relation->rd_att), values);
	}

	/* Clean up. */
	if (resultset)
	{
		DBCloseResultSet_table[version_idx](resultset, err_buf, sizeof(err_buf));
		resultset = NULL;
	}
	MemoryContextDelete(tupcontext);

	pfree(values);

	/*
	 * Step 2: Gather num_rows information:
	 * We use DESCRIBE EXTENDED for this. Should be much faster than running
	 * a COUNT(1) from table. Also note that COMPUTE STATISTICS has also been
	 * just run before calling this function
	 */
	*totalrows = 0;

	resetStringInfo(&sql);
	appendStringInfo(&sql, "%s %s", "DESCRIBE EXTENDED", svr_table);

	*totalrows = (double) hadoopGetInfoDescribe(conn, sql.data,
											"numRows", &found);

	/* dead rows returned as 0 always */
	*totaldeadrows = 0;

	/* cleanup resultset and the connection as well */
	if (resultset)
		DBCloseResultSet_table[version_idx](resultset, err_buf, sizeof(err_buf));

	DBCloseConnection_table[version_idx](conn, err_buf, sizeof(err_buf));

	/*
	 * Emit some interesting relation info
	 */
	if (!found)
		ereport(elevel,
			(errmsg("Total number of rows information not retrieved from hadoop...")));

	ereport(elevel,
			(errmsg("\"%s\": Hive table contains %.0f rows; "
					"%d rows in sample",
					RelationGetRelationName(relation),
					*totalrows, numrows)));

	elog(DEBUG2,"End: hadoopAcquireSampleRowsFunc");
	return numrows;
}

/*
 * Search for the "key" in the output of DESCRIBE EXTENDED
 */
static long
hadoopGetInfoDescribe(HiveConnection *conn, char *describe_query,
					   char *key, bool *found)
{
	size_t			col_count;
	size_t			col_len;
	size_t			data_byte_size;
	char		   *field;
	int				is_null_value;
	int				col_idx;
	long			result = 0;
	char			err_buf[MAX_HIVE_ERR_MSG_LEN];
	HiveResultSet  *resultset = NULL;

	*found = false;

	if (DBExecute_table[version_idx](conn,
					  describe_query,
					  &resultset,
					  1,
					  err_buf,
					  sizeof(err_buf)) == HIVE_ERROR)
	{
		char *err = pstrdup(err_buf);
		DBCloseConnection_table[version_idx](conn, err_buf, sizeof(err_buf));
		conn = NULL;
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
				 errmsg("failed to execute the Hive query: %s", err)
				));
	}

	/* get the records */
	while (DBFetch_table[version_idx](resultset, err_buf, sizeof(err_buf)) == HIVE_SUCCESS)
	{
		DBGetColumnCount_table[version_idx](resultset, &col_count, err_buf, sizeof(err_buf));
		for (col_idx = 0; col_idx < col_count; col_idx++)
		{
			DBGetFieldDataLen_table[version_idx](resultset, col_idx, &col_len, err_buf, sizeof(err_buf));

			field = (char *) palloc0(col_len + 1);
			if (DBGetFieldAsCString_table[version_idx](resultset,
									col_idx,
									field,
									col_len + 1,
									&data_byte_size,
									&is_null_value,
									err_buf,
									sizeof(err_buf)) == HIVE_ERROR)
			{
				char *err = pstrdup(err_buf);
				DBCloseConnection_table[version_idx](conn, err_buf, sizeof(err_buf));
				conn = NULL;
				ereport(ERROR,
						(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
						 errmsg("failed to get data from Hive query: %s", err)
						));

			}
			if (!is_null_value)
			{
				char	*needle;

				/* search if it contains what we are interested in */
				if ((needle = strstr(field, key)) != NULL)
				{
					elog(DEBUG2, "Found the param :%s", needle);
					needle += strlen(key) + 1; /* +1 for '=' */
					result = (double)strtoul(needle, NULL, 10);
					elog(DEBUG2, "converted received %s to long: %ld",
						 key, result);
					*found = true;
				}
			}
			/* free up early after peeking into it above */
			pfree(field);
		}
	}

	return result;
}

/*
 * hadoopAddForeignUpdateTargets
 *		Add column(s) needed for update/delete on a foreign table
 */
static void
hadoopAddForeignUpdateTargets(Query *parsetree,
								RangeTblEntry *target_rte,
								Relation target_relation)
{
	Var		   *var;
	const char *attrname;
	TargetEntry *tle;

	/*
	 * What we need is the rowid which is the first column
	 */
	Form_pg_attribute attr =
				RelationGetDescr(target_relation)->attrs[0];

	/* Make a Var representing the desired value */
	var = makeVar(parsetree->resultRelation,
				  1,
				  attr->atttypid,
				  attr->atttypmod,
				  InvalidOid,
				  0);

	/* Wrap it in a TLE with the right name ... */
	attrname = NameStr(attr->attname);

	tle = makeTargetEntry((Expr *) var,
						  list_length(parsetree->targetList) + 1,
						  pstrdup(attrname),
						  true);

	/* ... and add it to the query's targetlist */
	parsetree->targetList = lappend(parsetree->targetList, tle);
}

static List *
hadoopPlanForeignModify(PlannerInfo *root,
						  ModifyTable *plan,
						  Index resultRelation,
						  int subplan_index)
{
	CmdType			operation = plan->operation;
	RangeTblEntry  *rte = planner_rt_fetch(resultRelation, root);
	Relation		rel;
	StringInfoData	data;
	List		   *targetAttrs = NIL;

	elog(DEBUG2, "Start: hadoopPlanForeignModify");

	initStringInfo(&data);

	if (!hadoopIsDMLCapable(rte->relid))
		elog(ERROR, "Writes have not been enabled for this table");

	/*
	 * Core code already has some lock on each rel being planned, so we can
	 * use NoLock here.
	 */
	rel = heap_open(rte->relid, NoLock);

	if (operation == CMD_INSERT)
	{
		TupleDesc	tupdesc = RelationGetDescr(rel);
		int			attnum;

		for (attnum = 1; attnum <= tupdesc->natts; attnum++)
		{
			Form_pg_attribute attr = tupdesc->attrs[attnum - 1];

			if (!attr->attisdropped)
				targetAttrs = lappend_int(targetAttrs, attnum);
		}
	}
	else if (operation == CMD_UPDATE)
	{
		Bitmapset  *tmpset = bms_copy(rte->modifiedCols);
		AttrNumber	col;

		while ((col = bms_first_member(tmpset)) >= 0)
		{
			col += FirstLowInvalidHeapAttributeNumber;
			if (col <= InvalidAttrNumber)		/* shouldn't happen */
				elog(ERROR, "system-column update is not supported");

			/*
			 * We also disallow updates to the first column which
			 * happens to be the row identifier in Hbase
			 */
			if (col == 1)		/* shouldn't happen */
				elog(ERROR, "Hbase row identifier column update is not supported");

			targetAttrs = lappend_int(targetAttrs, col);
		}

		/* We also want the rowid column to be available for the update */
		targetAttrs = lcons_int(1, targetAttrs);
	}
	else if (operation == CMD_DELETE)
	{
		/* We also want the rowid column to be available for the delete */
		targetAttrs = lcons_int(1, targetAttrs);
	}

	/*
	 * RETURNING list not supported
	 */
	if (plan->returningLists)
		elog(ERROR, "RETURNING is not supported by this FDW");

	heap_close(rel, NoLock);

	elog(DEBUG2, "End: hadoopPlanForeignModify");

	return list_make1(targetAttrs);
}

static void
hadoopBeginForeignModify(ModifyTableState *mtstate,
						   ResultRelInfo *resultRelInfo,
						   List *fdw_private,
						   int subplan_index,
						   int eflags)
{
	hadoopFdwModifyState *fmstate;
	Relation			rel = resultRelInfo->ri_RelationDesc;
	ListCell		   *lc;
	Oid					typefnoid;
	bool				isvarlena;
	char			   *hbase_address = NULL;
	char			   *hbase_port = NULL;
	char			   *hbase_mapping = NULL;
	char			   *flume_address = NULL;
	char			   *flume_port = NULL;
	char			   *table = NULL;
	char				err_buf[256];

	elog(DEBUG2, "Start: hadoopBeginForeignModify");

	/*
	 * Do nothing in EXPLAIN (no ANALYZE) case.  resultRelInfo->ri_FdwState
	 * stays NULL.
	 */
	if (eflags & EXEC_FLAG_EXPLAIN_ONLY)
		return;

	/* Begin constructing hadoopFdwModifyState. */
	fmstate = (hadoopFdwModifyState *) palloc0(sizeof(hadoopFdwModifyState));
	fmstate->rel = rel;

	fmstate->target_attrs = (List *) list_nth(fdw_private, 0);

	/*
	 * Check if the table has "hbase" options first. If not
	 * found then check for "flume" options
	 */
	hadoopGetHbaseOptions(RelationGetRelid(rel), &hbase_address,
						&hbase_port, &hbase_mapping, &table);

	if (hbase_address == NULL && hbase_port == NULL)
	{
		fmstate->use_flume = true;

		/* Get a Connection to the Flume Server */
		hadoopGetFlumeOptions(RelationGetRelid(rel),
							&flume_address, &flume_port, &table);
		fmstate->fl_conn = flumeConnect(flume_address, flume_port);

		if (fmstate->fl_conn->status != FLUME_CONNECTION_OK)
			ereport(ERROR,
					(errcode(ERRCODE_FDW_UNABLE_TO_ESTABLISH_CONNECTION),
					 errmsg("Error Connecting to the Flume Server: %s",
							fmstate->fl_conn->errorMessage)));

	}
	else /* use HBASE for the INSERT operation */
	{
		/*
		 * The hbase_mapping field has to be present, error out
		 * otherwise
		 */
		if (hbase_mapping == NULL)
			ereport(ERROR,
					(errcode(ERRCODE_FDW_OPTION_NAME_NOT_FOUND),
					 errmsg("hbase_mapping option not available on remote table: %s", table)
					));

		/*
		 * Fill up defaults appropriately. Either can be NULL, not both at
		 * the same time
		 */
		if (hbase_address == NULL)
			hbase_address = (char *)DEFAULT_HBASE_HOST;

		if (hbase_port == NULL)
			hbase_port = (char *)DEFAULT_HBASE_PORT;

		/* Get a Connection to the Hbase Thrift Server */
		fmstate->hb_conn = HbaseOpenConnection(hbase_address,
							atoi(hbase_port), false, err_buf, sizeof(err_buf));

		if (fmstate->hb_conn == NULL)
			ereport(ERROR,
					(errcode(ERRCODE_FDW_UNABLE_TO_ESTABLISH_CONNECTION),
					 errmsg("failed to connect to Hbase: %s", err_buf)
					));

		fmstate->use_hbase = true;

		/* save the hbase_mapping info */
		fmstate->hbase_mapping = pstrdup(hbase_mapping);
		fmstate->hbase_table = pstrdup(table);
	}

	fmstate->p_flinfo = (FmgrInfo *) palloc0(
											 sizeof(FmgrInfo) * list_length(fmstate->target_attrs));

	fmstate->p_nums = 0;
	foreach(lc, fmstate->target_attrs)
	{
		int attnum = lfirst_int(lc);
		Form_pg_attribute attr = RelationGetDescr(rel)->attrs[attnum - 1];

		getTypeOutputInfo(attr->atttypid, &typefnoid, &isvarlena);
		fmgr_info(typefnoid, &fmstate->p_flinfo[fmstate->p_nums]);
		fmstate->p_nums++;
	}

	resultRelInfo->ri_FdwState = fmstate;

	elog(DEBUG2, "End: hadoopBeginForeignModify");
}

static TupleTableSlot *
hadoopExecForeignInsert(EState *estate,
						  ResultRelInfo *resultRelInfo,
						  TupleTableSlot *slot,
						  TupleTableSlot *planSlot)
{
	hadoopFdwModifyState *fmstate =
				(hadoopFdwModifyState *) resultRelInfo->ri_FdwState;
	int					n_rows = 0;

	elog(DEBUG2, "Start: hadoopExecForeignInsert");

	if (fmstate->use_hbase)
	{
		char   **hbase_cols = NULL;
		char   **hbase_vals = NULL;
		char   *row_id;
		int		nvals = 0;
		char	err_buf[256];

		hadoopGetHbaseValues(fmstate, slot, &row_id, &hbase_cols, &hbase_vals, &nvals);

		if (!HbaseMutateRow(fmstate->hb_conn, fmstate->hbase_table, row_id,
					   hbase_cols, hbase_vals, nvals,
					   err_buf, sizeof(err_buf)))
			ereport(ERROR,
					(errcode(ERRCODE_FDW_ERROR),
					 errmsg("insert/update failed: %s", err_buf)
					));

		n_rows = 1;
	}
	else /* use_flume */
	{
		StringInfoData		data;
		initStringInfo(&data);

		Assert(fmstate->use_flume);

		hadoopGetDataValues(&data, fmstate, slot);

		elog(DEBUG1, "hadoopExecForeignInsert: Sending Data: %s", data.data);

		n_rows = flumePutMessage(fmstate->fl_conn, data.data);
	}

	elog(DEBUG2,"End: hadoopExecForeignInsert");

	/* Return NULL if nothing was inserted on the remote end */
	return (n_rows > 0) ? slot : NULL;
}

/*
 * hadoopExecForeignUpdate
 *		Update one row in a foreign table
 */
static TupleTableSlot *
hadoopExecForeignUpdate(EState *estate,
						  ResultRelInfo *resultRelInfo,
						  TupleTableSlot *slot,
						  TupleTableSlot *planSlot)
{
	hadoopFdwModifyState *fmstate =
				(hadoopFdwModifyState *) resultRelInfo->ri_FdwState;
	TupleTableSlot	   *return_slot;

	elog(DEBUG2, "Start: hadoopExecForeignUpdate");

	/*
	 * Only Hbase supports UPDATE, error out otherwise. Additionally
	 * the handling is exactly similar to the INSERT case
	 */
	if (fmstate->use_hbase)
		return_slot = hadoopExecForeignInsert(estate, resultRelInfo,
											slot, planSlot);
	else
		return_slot = NULL;

	elog(DEBUG2, "End: hadoopExecForeignUpdate");
	return return_slot;
}

static TupleTableSlot *
hadoopExecForeignDelete(EState *estate,
						  ResultRelInfo *resultRelInfo,
						  TupleTableSlot *slot,
						  TupleTableSlot *planSlot)
{
	hadoopFdwModifyState *fmstate =
				(hadoopFdwModifyState *) resultRelInfo->ri_FdwState;
	int					n_rows = 0;
	Datum				datum;
	bool				isNull;

	elog(DEBUG2, "Start: hadoopExecForeignDelete");

	if (fmstate->use_hbase)
	{
		char   *row_id;
		char	err_buf[256];
		Oid			typefnoid;
		FmgrInfo flinfo;
		bool		isvarlena;
		Form_pg_attribute attr = RelationGetDescr(fmstate->rel)->attrs[0];

		/* Get the id that was passed up as a resjunk column */
		datum = ExecGetJunkAttribute(planSlot,
									 1,
									 &isNull);

		getTypeOutputInfo(attr->atttypid, &typefnoid, &isvarlena);
		fmgr_info(typefnoid, &flinfo);

		row_id = OutputFunctionCall(&flinfo, datum);

		if (!HbaseDeleteRow(fmstate->hb_conn, fmstate->hbase_table, row_id,
					   err_buf, sizeof(err_buf)))
			ereport(ERROR,
					(errcode(ERRCODE_FDW_ERROR),
					 errmsg("delete failed: %s", err_buf)
					));

		n_rows = 1;
	}

	elog(DEBUG2,"End: hadoopExecForeignDelete");

	/* Return NULL if nothing was deleted on the remote end */
	return (n_rows > 0) ? slot : NULL;
}

static void
hadoopEndForeignModify(EState *estate,
						 ResultRelInfo *resultRelInfo)
{
	hadoopFdwModifyState *fmstate = (hadoopFdwModifyState *) resultRelInfo->ri_FdwState;

	elog(DEBUG2, "Start: hadoopEndForeignModify");

	/* If fmstate is NULL, we are in EXPLAIN; nothing to do */
	if (fmstate == NULL)
		return;

	/* Release remote connection */
	if (fmstate->use_flume)
	{
		flumeCloseConnection(fmstate->fl_conn);
		fmstate->fl_conn = NULL;
	}
	else if (fmstate->use_hbase)
	{
		char err_buf[256];

		HbaseCloseConnection(fmstate->hb_conn, err_buf, sizeof(err_buf));
		fmstate->hb_conn = NULL;
	}

	elog(DEBUG2, "End: hadoopEndForeignModify");
}

static void
hadoopExplainForeignModify(ModifyTableState *mtstate,
							 ResultRelInfo *rinfo,
							 List *fdw_private,
							 int subplan_index,
							 ExplainState *es)
{
	if (es->verbose)
	{
		char	   *data = strVal(list_nth(fdw_private, 0));

		ExplainPropertyText("Remote SET", data, es);
	}
}

static void
hadoopGetOptions(Oid foreigntableid, char **address, int *port, char **database, char **table)
{
	ForeignTable	*f_table;
	ForeignServer	*f_server;
	List		*options;
	ListCell	*lc;

	elog(DEBUG2, "Start: hadoopGetOptions");

	/*
	 * Extract options from FDW objects.
	 */
	f_table = GetForeignTable(foreigntableid);
	f_server = GetForeignServer(f_table->serverid);

	options = NIL;
	options = list_concat(options, f_table->options);

	/* Loop through the options, and get the table name */
	foreach(lc, options)
	{
		DefElem *def = (DefElem *) lfirst(lc);

		if (strcmp(def->defname, "table") == 0)
			*table = defGetString(def);
	}

	/* Get the server options now */
	hadoopGetServerOptions(f_table->serverid, address, port, database);

	elog(DEBUG2, "End: hadoopGetOptions");
}

static void
hadoopGetServerOptions(Oid serverid, char **address, int *port, char **database)
{
	ForeignServer	*f_server;
	List		*options;
	ListCell	*lc;

	elog(DEBUG2, "Start: hadoopGetServerOptions");

	/*
	 * Extract options from FDW objects.
	 */
	f_server = GetForeignServer(serverid);

	options = NIL;
	options = list_concat(options, f_server->options);

	/* Loop through the options, and get the server/port */
	foreach(lc, options)
	{
		DefElem *def = (DefElem *) lfirst(lc);

		if (strcmp(def->defname, "address") == 0)
			*address = defGetString(def);

		if (strcmp(def->defname, "port") == 0)
			*port = atoi(defGetString(def));

		if (strcmp(def->defname, "database") == 0)
			*database = defGetString(def);
	}

	/* Default values, if required */
	if (!*address) {
		*address = (char *) palloc(strlen(DEFAULT_HOST));
		snprintf(*address, strlen(DEFAULT_HOST), "%s", DEFAULT_HOST);
	}

	if (!*port)
		*port = atoi(DEFAULT_PORT);

	if (!*database) {
		*database = (char *) palloc(strlen(DEFAULT_DATABASE));
		snprintf(*database, strlen(DEFAULT_DATABASE), "%s", DEFAULT_DATABASE);
	}

	elog(DEBUG2, "End: hadoopGetServerOptions");
}

static void
hadoopGetFlumeOptions(Oid foreigntableid, char **flume_address,
					char **flume_port, char **table)
{
	ForeignTable	*f_table;
	ForeignServer	*f_server;
	List		*options;
	ListCell	*lc;
	char		*address = NULL;

	elog(DEBUG2, "Start: hadoopFlumeGetOptions");

	/*
	 * Extract options from FDW objects.
	 */
	f_table = GetForeignTable(foreigntableid);
	f_server = GetForeignServer(f_table->serverid);

	options = NIL;
	options = list_concat(options, f_table->options);
	options = list_concat(options, f_server->options);

	/* Loop through the options, and get the server/port */
	foreach(lc, options)
	{
		DefElem *def = (DefElem *) lfirst(lc);

		if (strcmp(def->defname, "address") == 0)
			address = defGetString(def);

		if (strcmp(def->defname, "flume_address") == 0)
			*flume_address = defGetString(def);

		if (strcmp(def->defname, "table") == 0)
			*table = defGetString(def);

		if (strcmp(def->defname, "flume_port") == 0)
			*flume_port = defGetString(def);
	}

	/* Default values, if required */
	if (!address) {
		address = (char *) palloc(strlen(DEFAULT_HOST) + 1);
		snprintf(address, strlen(DEFAULT_HOST), "%s", DEFAULT_HOST);
	}

	elog(DEBUG2, "hadoopGetFlumeOptions: flume host address %s", address);

	if (!*flume_address) {
		*flume_address = (char *) palloc(strlen(address) + 1);
		snprintf(*flume_address, strlen(address) + 1, "%s", address);
	}

	elog(DEBUG2, "End: hadoopGetFlumeOptions");
}

static void
hadoopGetHbaseOptions(Oid foreigntableid, char **hbase_address,
					char **hbase_port, char **hbase_mapping, char **table)
{
	ForeignTable	*f_table;
	List		*options;
	ListCell	*lc;

	elog(DEBUG2, "Start: hadoopGetHbaseOptions");

	/*
	 * Extract options from FDW objects.
	 */
	f_table = GetForeignTable(foreigntableid);

	options = f_table->options;

	/* Loop through the options, and get the server/port */
	foreach(lc, options)
	{
		DefElem *def = (DefElem *) lfirst(lc);

		if (strcmp(def->defname, "hbase_address") == 0)
			*hbase_address = defGetString(def);

		if (strcmp(def->defname, "table") == 0)
			*table = defGetString(def);

		if (strcmp(def->defname, "hbase_port") == 0)
			*hbase_port = defGetString(def);

		if (strcmp(def->defname, "hbase_mapping") == 0)
			*hbase_mapping = defGetString(def);
	}

	elog(DEBUG2, "End: hadoopGetHbaseOptions");
}

/*
 * hadoopGetHbaseValues
 *
 *	Get Hbase column names and corresponding values in two separate
 *	arrays. We also return the size of this array
 *
 * slot is slot to get remaining parameters from, or NULL if none
 */
static void
hadoopGetHbaseValues(hadoopFdwModifyState *fmstate, TupleTableSlot *slot,
				  char **rowid, char ***hbase_cols, char ***hbase_vals, int *nvals)
{
	int			pindex = 0, col_idx, access_idx;
	ListCell   *lc;
	char	   **cols, **vals;
	char	   *rawstring;
	List	   *collist;
	ListCell   *l;
	bool		first;
	int			attnum;
	Datum		value;
	bool		isnull;

	elog(DEBUG2, "Start: hadoopGetHbaseValues");

	*nvals = 0;

	/* Need a modifiable copy of string */
	rawstring = pstrdup(fmstate->hbase_mapping);

	/*
	 * Convert the hbase_mapping string into a list of Hbase side
	 * column names
	 */
	if (!SplitIdentifierString(rawstring, ',', &collist))
	{
		/* syntax error in list */
		list_free(collist);
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
				 errmsg("Hbase column mapping is not in proper format: %s",
						fmstate->hbase_mapping)));
	}

	cols = (char **) palloc(sizeof(char *) *
						   list_length(fmstate->target_attrs));
	vals = (char **) palloc(sizeof(char *) *
						   list_length(fmstate->target_attrs));

	/* This has to move appropriately based on the attrs being chosen */
	l = list_head(collist);
	access_idx = 0;
	col_idx = 0;

	/*
	 * We always fetch the rowid in Hbase and it's the first column, so
	 * fetch it immediately now because in cases like DELETE, the
	 * target_attrs will be NIL
	 */
	value = slot_getattr(slot, 1, &isnull);

	/* The first attribute should be the rowid/key */
	*rowid = OutputFunctionCall(&fmstate->p_flinfo[pindex++], value);

	first = true; /* to allow skipping from target_attrs */
	foreach(lc, fmstate->target_attrs)
	{
		attnum = lfirst_int(lc);

		while (access_idx + 1 < attnum)
		{
			l = lnext(l);
			access_idx++;
		}

		/* The first attribute should be the rowid/key */
		if (first)
		{
			first = false;
			continue;
		}

		value = slot_getattr(slot, attnum, &isnull);

		/* We do not store NULLs in Hbase */
		if (!isnull)
		{
			cols[col_idx] = (char *) lfirst(l);
			vals[col_idx++] = OutputFunctionCall(&fmstate->p_flinfo[pindex], value);
		}

		pindex++;
	}

	Assert(pindex == fmstate->p_nums);

	*hbase_cols = cols;
	*hbase_vals = vals;
	*nvals = col_idx;
	elog(DEBUG2, "End: hadoopGetHbaseValues");
}

/*
 * hadoopGetDataValues
 *		Create a CSV string of the values
 *
 * slot is slot to get remaining parameters from, or NULL if none
 */
static void
hadoopGetDataValues(StringInfo buf, hadoopFdwModifyState *fmstate, TupleTableSlot *slot)
{
	int			pindex = 0;
	ListCell   *lc;
	bool		first;

	elog(DEBUG2, "Start: hadoopGetDataValues");

	first = true;
	foreach(lc, fmstate->target_attrs)
	{
		int			attnum = lfirst_int(lc);
		Datum		value;
		bool		isnull;

		value = slot_getattr(slot, attnum, &isnull);

		if (!first)
			appendStringInfoString(buf, ",");
		first = false;

		if (!isnull)
			appendStringInfo(buf, "%s",
				OutputFunctionCall(&fmstate->p_flinfo[pindex], value));

		pindex++;
	}

	Assert(pindex == fmstate->p_nums);

	elog(DEBUG2, "End: hadoopGetDataValues");
}

/*
 * This checks if the foreign table is capable of supporting INSERTs,
 * UPDATEs and DELETEs
 *
 * Hbase gets precedence to Flume for DML operations
 */
static bool
hadoopIsDMLCapable(Oid foreigntableid)
{
	ForeignTable	*f_table;
	List		*options;
	ListCell	*lc;

	elog(DEBUG2, "Start: hadoopIsDMLCapable");

	/*
	 * Extract options from FDW objects.
	 */
	f_table = GetForeignTable(foreigntableid);

	options = f_table->options;

	/*
	 * Loop through the options, and return TRUE if
	 * flume_address, flume_port, hbase_address, hbase_port
	 * values are observed
	 */
	foreach(lc, options)
	{
		DefElem *def = (DefElem *) lfirst(lc);

		if (strcmp(def->defname, "flume_address") == 0)
			return true;

		if (strcmp(def->defname, "flume_port") == 0)
			return true;

		if (strcmp(def->defname, "hbase_address") == 0)
			return true;

		if (strcmp(def->defname, "hbase_port") == 0)
			return true;
	}

	elog(DEBUG2, "End: hadoopIsDMLCapable");
	return false;
}

Datum
hadoop_fdw_create_table(PG_FUNCTION_ARGS)
{
	text	*server_name = PG_GETARG_TEXT_P(0);
	text	*hadoop_table_name = PG_GETARG_TEXT_P(1);
	text	*postgres_table_name;
	char	*postgres_table;

	ForeignServer *server;
	char			*svr_address = NULL;
	int				svr_port = 0;
	char			*svr_database = NULL;
	char			err_buf[MAX_HIVE_ERR_MSG_LEN];
	StringInfoData	sql, localsql, dropsql;
	HiveConnection *conn;
	AclResult		aclresult;
	HiveResultSet*	resultset;
	bool			first;
	size_t			col_count;
	size_t			col_len;
	size_t			data_byte_size;
	char		   *field, *dtype, *column;
	int				is_null_value;
	int				col_idx;

	/*
	 * Check that the foreign server exists and that we have CREATE on it
	 */
	server = GetForeignServerByName(text_to_cstring(server_name), false);
	aclresult = pg_foreign_server_aclcheck(server->serverid,
										   GetUserId(), ACL_CREATE);
	if (aclresult != ACLCHECK_OK)
		aclcheck_error(aclresult, ACL_KIND_FOREIGN_SERVER, server->servername);

	/*
	 * Connect to Hive to run the DESCRIBE command
	 */
	hadoopGetServerOptions(server->serverid, &svr_address, &svr_port, &svr_database);

	/* Determine target HiveServer version */
	version_idx = hive_version - 1;
	if (version_idx < 0 || version_idx > 1)
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_ESTABLISH_CONNECTION),
				 errmsg("invalid hive version specified: %s", err_buf)
				));

	/* Connect to the server */
	conn = DBOpenConnection_table[version_idx](svr_database,
							svr_address,
							svr_port,
							atoi(DEFAULT_FRAMED),
							err_buf,
							sizeof(err_buf));

	if (!conn)
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_ESTABLISH_CONNECTION),
				 errmsg("failed to connect to Hive: %s", err_buf)
				));

	initStringInfo(&sql);
	appendStringInfo(&sql, "%s %s", "DESCRIBE",
							text_to_cstring(hadoop_table_name));

	/* Get the local postgres foreign table name */
	if (PG_NARGS() == 3)
	{
		postgres_table_name = PG_GETARG_TEXT_P(2);

		postgres_table = text_to_cstring(postgres_table_name);
	}
	else
	{
		/* Name defaults to Hive name, minus the schema */
		List *namelist = NIL;

		SplitIdentifierString(text_to_cstring(hadoop_table_name), '.',
							  &namelist);

		if (list_length(namelist) == 1)
			postgres_table = text_to_cstring(hadoop_table_name);
		else if (list_length(namelist) == 2)
			postgres_table = (char *)lsecond(namelist);
		else /* shouldn't be more than one dot in the name */
			ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
				 errmsg("Hive table name is not in proper format: %s",
				text_to_cstring(hadoop_table_name))));
	}

	/*
	 * As we get the columns and types, we also generate a corresponding
	 * local foreign table create statement to match
	 *
	 * But first we add a DROP IF EXISTS as well
	 */
	initStringInfo(&localsql);
	initStringInfo(&dropsql);
	appendStringInfo(&dropsql, "%s ", "DROP FOREIGN TABLE IF EXISTS");
	appendStringInfo(&dropsql, "%s;", postgres_table);
	appendStringInfo(&localsql, "%s ", "CREATE FOREIGN TABLE");
	appendStringInfo(&localsql, "%s", postgres_table);

	elog(DEBUG1,"hadoop_fdw: describing remote object: %s", sql.data);
	if (DBExecute_table[version_idx](conn,
					  sql.data,
					  &resultset,
					  1,
					  err_buf,
					  sizeof(err_buf)) == HIVE_ERROR)
	{
		char *err = pstrdup(err_buf);
		DBCloseConnection_table[version_idx](conn, err_buf, sizeof(err_buf));
		conn = NULL;
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
				 errmsg("failed to execute the Hive query: %s", err)
				));
	}

	appendStringInfo(&localsql, " (");

	first = true;
	/* get the record */
	while (DBFetch_table[version_idx](resultset, err_buf, sizeof(err_buf)) == HIVE_SUCCESS)
	{
		DBGetColumnCount_table[version_idx](resultset, &col_count, err_buf, sizeof(err_buf));

		/*
		 * 1st column is col_name, 2nd is data_type, 3rd comments. We
		 * ignore the 3rd
		 */
		if (col_count != 3)
			ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
				 errmsg("Hive DESCRIBE query output has changed!")));

		if (!first)
			appendStringInfoString(&localsql, ",\n");
		first = false;

		for (col_idx = 0; col_idx < (col_count - 1); col_idx++)
		{
			DBGetFieldDataLen_table[version_idx](resultset, col_idx, &col_len, err_buf, sizeof(err_buf));

			column = (char *) palloc0(col_len + 1);
			if (DBGetFieldAsCString_table[version_idx](resultset,
									col_idx,
									column,
									col_len + 1,
									&data_byte_size,
									&is_null_value,
									err_buf,
									sizeof(err_buf)) == HIVE_ERROR)
			{
				char *err = pstrdup(err_buf);
				DBCloseConnection_table[version_idx](conn, err_buf, sizeof(err_buf));
				conn = NULL;
				ereport(ERROR,
						(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
						 errmsg("failed to get data from Hive query: %s", err)
						));

			}

			if (!is_null_value)
			{
				if (col_idx == 0) /* column_name */
				{
					/*
					 * Hive returns right space padded col names, so
					 * we use strtok to split around blanks and use the first
					 * field.
					 */
					field = strtok(column, " ");
					appendStringInfoString(&localsql, quote_identifier(column));
				}
				else /* data_type */
				{
					/*
					 * Need to convert Hive data type into a corresponding
					 * PG data type. The list of Hive data types is small
					 * and DDL will happen only once in a while, so adding
					 * multiple IF checks is OK for now.
					 *
					 * Hive returns right space padded datatypes, so
					 * we use strtok to split around blanks and use the first
					 * field.
					 */
					field = strtok(column, " ");

					if (strcmp(field, "tinyint") == 0)
						dtype = "smallint";
					else if (strcmp(field, "smallint") == 0)
						dtype = "smallint";
					else if (strcmp(field, "int") == 0)
						dtype = "int";
					else if (strcmp(field, "bigint") == 0)
						dtype = "bigint";
					else if (strcmp(field, "boolean") == 0)
						dtype = "boolean";
					else if (strcmp(field, "float") == 0)
						dtype = "float";
					else if (strcmp(field, "double") == 0)
						dtype = "double precision";
					else if (strcmp(field, "string") == 0)
						dtype = "varchar";
					else if (strcmp(field, "binary") == 0)
						dtype = "bytea";
					else if (strcmp(field, "timestamp") == 0)
						dtype = "timestamp";
					else if (strcmp(field, "decimal") == 0)
						dtype = "decimal";
					else	/* We need to error out for unsupported data types */
					{
						DBCloseConnection_table[version_idx](conn, err_buf, sizeof(err_buf));
						conn = NULL;
						ereport(ERROR,
							(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
							 errmsg("Unsupported Hive Data Type %s", field)
							));
					}

					appendStringInfo(&localsql, " %s", dtype);
				}
			}
			else /* should not happen */
			{
				DBCloseConnection_table[version_idx](conn, err_buf, sizeof(err_buf));
				conn = NULL;
				ereport(ERROR,
					(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
					 errmsg("Unexpected NULL data from Hive DESCRIBE query")
					));
			}

			/* free up early */
			pfree(column);
		}
	}

	appendStringInfo(&localsql, ")\nSERVER %s", server->servername);

	/* add OPTIONS */
	appendStringInfo(&localsql, " OPTIONS (table '%s')",
					 text_to_cstring(hadoop_table_name));

	elog(DEBUG1, "The constructed sql is %s", localsql.data);

	/* cleanup resultset and the connection as well */
	if (resultset)
		DBCloseResultSet_table[version_idx](resultset, err_buf, sizeof(err_buf));

	DBCloseConnection_table[version_idx](conn, err_buf, sizeof(err_buf));

	/*
	 * Execute this query locally via SPI now
	 * We execute a DROP IF EXISTS first locally before firing
	 * off the CREATE
	 */
	hadoopExecuteSPI(dropsql.data);
	hadoopExecuteSPI(localsql.data);

	PG_RETURN_BOOL(true);
}

Datum
hadoop_fdw_create_view(PG_FUNCTION_ARGS)
{
	text	*server_name = PG_GETARG_TEXT_P(0);
	text	*hadoop_view_name = PG_GETARG_TEXT_P(1);
	text	*hadoop_view_query = PG_GETARG_TEXT_P(2);
	text	*postgres_view_name;
	char	*postgres_view;

	ForeignServer *server;
	char			*svr_address = NULL;
	int				svr_port = 0;
	char			*svr_database = NULL;
	char			err_buf[MAX_HIVE_ERR_MSG_LEN];
	StringInfoData	sql;
	HiveConnection *conn;
	AclResult		aclresult;
	HiveResultSet*	resultset;

	/*
	 * Check that the foreign server exists and that we have CREATE on it
	 */
	server = GetForeignServerByName(text_to_cstring(server_name), false);
	aclresult = pg_foreign_server_aclcheck(server->serverid,
										   GetUserId(), ACL_CREATE);
	if (aclresult != ACLCHECK_OK)
		aclcheck_error(aclresult, ACL_KIND_FOREIGN_SERVER, server->servername);

	/*
	 * Connect to Hive to run the view query
	 */
	hadoopGetServerOptions(server->serverid, &svr_address, &svr_port, &svr_database);

	/* Determine target HiveServer version */
	version_idx = hive_version - 1;
	if (version_idx < 0 || version_idx > 1)
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_ESTABLISH_CONNECTION),
				 errmsg("invalid hive version specified: %s", err_buf)
				));

	/* Connect to the server */
	conn = DBOpenConnection_table[version_idx](svr_database,
							svr_address,
							svr_port,
							atoi(DEFAULT_FRAMED),
							err_buf,
							sizeof(err_buf));

	if (!conn)
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_ESTABLISH_CONNECTION),
				 errmsg("failed to connect to Hive: %s", err_buf)
				));

	initStringInfo(&sql);

	/*
	 * DROP the view if it exists. Note that this is not transactional.
	 * So if the DROP succeeds and the subsequent CREATE VIEW fails we
	 * cannot do much...
	 */
	appendStringInfo(&sql, "DROP VIEW IF EXISTS %s",
					 text_to_cstring(hadoop_view_name));
	elog(DEBUG1,
		 "hadoop_fdw: dropping Hive view: %s", sql.data);
	if (DBExecute_table[version_idx](conn,
					  sql.data,
					  &resultset,
					  1,
					  err_buf,
					  sizeof(err_buf)) == HIVE_ERROR)
	{
		char *err = pstrdup(err_buf);
		DBCloseConnection_table[version_idx](conn, err_buf, sizeof(err_buf));
		conn = NULL;
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
				 errmsg("failed to execute the Hive query: %s", err)
				));
	}

	resetStringInfo(&sql);
	appendStringInfo(&sql, "%s %s %s %s", "CREATE VIEW",
			text_to_cstring(hadoop_view_name), "AS",
			text_to_cstring(hadoop_view_query));

	if (PG_NARGS() == 4)
	{
		postgres_view_name = PG_GETARG_TEXT_P(3);
		postgres_view = text_to_cstring(postgres_view_name);
	}
	else
	{
		/* Name defaults to Hive name, minus the schema */
		List *namelist = NIL;

		SplitIdentifierString(text_to_cstring(hadoop_view_name), '.',
							  &namelist);

		if (list_length(namelist) == 1)
			postgres_view = text_to_cstring(hadoop_view_name);
		else if (list_length(namelist) == 2)
			postgres_view = (char *)lsecond(namelist);
		else /* shouldn't be more than one dot in the name */
			ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
				 errmsg("Hive view name is not in proper format: %s",
				text_to_cstring(hadoop_view_name))));
	}

	elog(DEBUG1,"hadoop_fdw: creating Hive view: %s", sql.data);
	if (DBExecute_table[version_idx](conn,
					  sql.data,
					  &resultset,
					  1,
					  err_buf,
					  sizeof(err_buf)) == HIVE_ERROR)
	{
		char *err = pstrdup(err_buf);
		DBCloseConnection_table[version_idx](conn, err_buf, sizeof(err_buf));
		conn = NULL;
		ereport(ERROR,
				(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
				 errmsg("failed to execute the Hive query: %s", err)
				));
	}

	DBCloseConnection_table[version_idx](conn, err_buf, sizeof(err_buf));

	/*
	 * The above command created the view on the Hive side, create the
	 * corresponding local foreign table now. We use the existing
	 * hadoop_fdw_create_table functionality for this
	 */

	PG_RETURN_BOOL(DirectFunctionCall3Coll(hadoop_fdw_create_table, PG_GET_COLLATION(),
		PointerGetDatum(server_name), PointerGetDatum(hadoop_view_name),
		PointerGetDatum(cstring_to_text(postgres_view))));
}

/*
 * Transactionally EXECUTE the Hive local Utility command
 */
static void
hadoopExecuteSPI(char *query)
{
	int		ret;

	SPI_connect();

	ret = SPI_execute(query, false, 0);

	if (ret != SPI_OK_UTILITY)
		ereport(ERROR,
			(errcode(ERRCODE_FDW_UNABLE_TO_CREATE_EXECUTION),
			 errmsg("Unable to execute Hive DDL query: %s", query)
			));

	SPI_finish();
}
