/*-------------------------------------------------------------------------
 *
 * flume_client.c
 *                A Socket Client for Flume
 *
 * Copyright (c) 2012-2013, BigSQL Development Group
 *
 * IDENTIFICATION
 *                hadoop_fdw/src/flume_client.c
 *
 *-------------------------------------------------------------------------
 */

#include "postgres.h"

#include "flume_client.h"

static FlumeConnection *initializeConnection(void);
static int putMsgBytes(const void *buf, size_t len, FlumeConnection *conn);
static int flume_flush(FlumeConnection *conn);


FlumeConnection *
flumeConnect(char* address, char* port)
{
	FlumeConnection	*conn;
	struct addrinfo *addrs = NULL;
	struct addrinfo hint;
	int 		ret;

	conn = initializeConnection();
	if (conn == NULL)
		return NULL;

	if (!address)
	{
		conn->status = FLUME_CONNECTION_BAD;
		strcpy(conn->errorMessage, "Address required");
		return conn;
	}

	if (!port)
	{
		conn->status = FLUME_CONNECTION_BAD;
		strcpy(conn->errorMessage, "Port required");
		return conn;
	}

	conn->host = strdup(address);
	conn->port = strdup(port);

	conn->outCount = 0;
	conn->outMsgEnd = 0;

	/* Initialize hint structure */
	MemSet(&hint, 0, sizeof(hint));
	hint.ai_socktype = SOCK_STREAM;
	hint.ai_family = AF_UNSPEC;
	hint.ai_flags = AI_NUMERICHOST;

	ret = getaddrinfo(conn->host, conn->port, &hint, &addrs);
	if (ret || !addrs)
	{
		conn->status = FLUME_CONNECTION_BAD;
		strcpy(conn->errorMessage, "could not translate address");

		if (addrs)
			freeaddrinfo(addrs);

		return conn;
	}

	conn->addrlist = addrs;
	conn->addr_cur = addrs;

	while (conn->addr_cur != NULL)
	{
		struct addrinfo *addr_cur = conn->addr_cur;

		/* Remember current address for possible error msg */
		memcpy(&conn->addr.addr, addr_cur->ai_addr,
			   addr_cur->ai_addrlen);
		conn->addr.salen = addr_cur->ai_addrlen;

		/* Open a socket */
		conn->sock = socket(addr_cur->ai_family, SOCK_STREAM, 0);
		if (conn->sock < 0)
		{
			/*
			 * ignore socket() failure if we have more addresses
			 * to try
			 */
			if (addr_cur->ai_next != NULL)
			{
				conn->addr_cur = addr_cur->ai_next;
				continue;
			}

			strcpy(conn->errorMessage, "could not create socket");
			conn->status = FLUME_CONNECTION_BAD;
			return conn;
		}

		if (connect(conn->sock, addr_cur->ai_addr,
					addr_cur->ai_addrlen) < 0)
		{
			strcpy(conn->errorMessage, "could not open connection to ");
			strcat(conn->errorMessage, address);
			strcat(conn->errorMessage, " on port ");
			strcat(conn->errorMessage, port);
			conn->status = FLUME_CONNECTION_BAD;
			return conn;
		}

		conn->status = FLUME_CONNECTION_OK;
		break;
	}

	return conn;
}

void
flumeCloseConnection(FlumeConnection *conn)
{
	/* Close the socket itself */
	if (conn->sock >= 0)
		close(conn->sock);

	conn->sock = -1;

	conn->inStart = 0;
	conn->outCount = 0;

	conn->status = FLUME_CONNECTION_BAD;

	freeaddrinfo(conn->addrlist);
	conn->addrlist = NULL;
	conn->addr_cur = NULL;
	pfree(conn);
}

int
flumePutMessage(FlumeConnection *conn, char *msg)
{
	if (!conn)
		return 0;

	/* clear the error string */
	memset(conn->errorMessage, 0, 1024);

	if (conn->status != FLUME_CONNECTION_OK)
	{
		strcpy(conn->errorMessage, "no connection to the server");
		return 0;
	}

	putMsgBytes(msg, strlen(msg) + 1, conn);
	putMsgBytes("\n", 2, conn);

	if (flume_flush(conn) > 0)
		return 1;
	else
		ereport(ERROR,
				(errcode_for_socket_access(),
				 errmsg("error sending on Flume socket connection")));

	return 0;
}

static FlumeConnection *
initializeConnection(void)
{
	FlumeConnection	   *conn;

	conn = (FlumeConnection *) palloc(sizeof(FlumeConnection));
	if (conn == NULL)
	{
		return conn;
	}

	MemSet(conn, 0, sizeof(FlumeConnection));

	conn->status = FLUME_CONNECTION_BAD;
	conn->sock = -1;

	conn->inBufSize = 16 * 1024;
	conn->inBuffer = (char *) palloc(conn->inBufSize);
	conn->outBufSize = 16 * 1024;
	conn->outBuffer = (char *) palloc(conn->outBufSize);
	conn->errorMsgSize = 1024;
	conn->errorMessage = (char *) palloc(1024);

	return conn;
}

static int
putMsgBytes(const void *buf, size_t len, FlumeConnection *conn)
{
	memcpy(conn->outBuffer + conn->outMsgEnd, buf, len);
	conn->outMsgEnd += len;

	return 0;
}

static int
flume_flush(FlumeConnection *conn)
{
	if (conn->outMsgEnd > 0)
	{
		int	rc;

		do
		{
			rc = send(conn->sock, conn->outBuffer,
									conn->outMsgEnd, 0);
		} while (rc < 0 && errno == EINTR);

		if (rc < 0)
			elog(LOG, "Flume send error: %d : %s",
							errno, strerror(errno));

		return rc;
	}

	return 0;
}
