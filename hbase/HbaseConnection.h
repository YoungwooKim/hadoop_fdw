/*
 *   Copyright (c) 2012-2013, BigSQL Development Group
 */
#ifndef __hbase_connection_h__
#define __hbase_connection_h__

#include <boost/shared_ptr.hpp>

using namespace boost;
using namespace apache::thrift::transport;
using namespace apache::hadoop::hbase::thrift;


/*************************************************************************************************
 * HiveConnection Class Definition
 ************************************************************************************************/

/*
 * Container class for Hbase connections.
 */

struct HbaseConnection {
  HbaseConnection(apache::hadoop::hbase::thrift::HbaseClient c, boost::shared_ptr<TTransport> t) :
    client(c), transport(t) {}
  apache::hadoop::hbase::thrift::HbaseClient client;
  boost::shared_ptr<TTransport> transport;
};
#endif // __hive_connection_h__
