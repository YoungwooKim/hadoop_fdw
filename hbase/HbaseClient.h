/*-------------------------------------------------------------------------
 *
 * HbaseClient.h
 *	Global Hbase Client Functions Headers (usable as C callback functions)
 *
 * Copyright (c) 2012-2013, BigSQL Development Group
 *
 * IDENTIFICATION
 *                hadoop_fdw/hbase/hbase_client/HbaseClient.h
 *
 *-------------------------------------------------------------------------
 */
#ifndef __hbase_client_h__
#define __hbase_client_h__

#ifndef __STDC_FORMAT_MACROS
#define __STDC_FORMAT_MACROS
#endif
#include <inttypes.h>
#include <stdint.h>

/******************************************************************************
 * Hbase Client C++ Class Placeholders
 *****************************************************************************/

/*
 * Enumeration of Hbase return values
 */
enum HbaseReturn
{
  HBASE_SUCCESS,
  HBASE_ERROR,
  HBASE_NO_MORE_DATA,
  HBASE_SUCCESS_WITH_MORE_DATA
};

/* Default Hbase Server host */
static const char *DEFAULT_HBASE_HOST     = "localhost";
/* Default Hbase Server port */
static const char *DEFAULT_HBASE_PORT     = "9090";

typedef enum HbaseReturn HbaseReturn;
typedef struct HbaseConnection HbaseConnection;

/*
 * Checks an error condition, and if true:
 * 1. prints the error
 * 2. saves the message to err_buf
 * 3. returns the specified ret_val
 */
#define RETURN_ON_ASSERT(condition, funct_name, error_msg, err_buf, err_buf_len, ret_val) {     \
  if (condition) {                                                                              \
      cerr << funct_name << ": " << error_msg << endl << flush;                                 \
      strncpy(err_buf, error_msg, err_buf_len);													\
      return ret_val;                                                                           \
  }                                                                                             \
}

/*
 * Always performs the following:
 * 1. prints the error
 * 2. saves the message to err_buf
 * 3. returns the specified ret_val
 */
#define RETURN_FAILURE(funct_name, error_msg, err_buf, err_buf_len, ret_val) {  \
  RETURN_ON_ASSERT(true, funct_name, error_msg, err_buf, err_buf_len, ret_val); \
}

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

HbaseConnection *HbaseOpenConnection(const char *host, int port, bool framed, char *err_buf,
							size_t err_buf_len);
HbaseReturn	HbaseCloseConnection(HbaseConnection *connection, char *err_buf, size_t err_buf_len);
bool HbaseMutateRow(HbaseConnection *connection, char *table, char *rowid, char **cols,
					char **vals, int nvals, char *err_buf, size_t err_buf_len);
bool HbaseDeleteRow(HbaseConnection *connection, char *table, char *rowid, char *err_buf,
					size_t err_buf_len);

#ifdef __cplusplus
} // extern "C"
#endif // __cpluscplus
#endif // __hbase_client_h__
